package common

const (
	GsErrorCodeNone int32 = iota
	GsErrorCodeRegisterFailed
)

const (
	GErrorCodeNone int32 = iota
	GErrorCodeParamError
	GErrorCodeInternalError
	GErrorCodeAccountValidityError
	GErrorCodeAccountBanLoginError
	GErrorCodeAccountDuplicateNameError
	GErrorCodeTooManyPlayerError
	GErrorCodePlayerDuplicateNameError
	GErrorCodeGuildDuplicateNameError
)

type HTTPRespBase struct {
	Err int    `json:"error"`
	Msg string `json:"message"`
}
