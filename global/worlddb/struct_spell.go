package worlddb

// SpellStageType
const (
	SpellStageType_Chant   = 0
	SpellStageType_Channel = 1
	SpellStageType_Cleanup = 2
)

// SpellResumeType
const (
	SpellResumeType_RemoveWhenOffline  = 0
	SpellResumeType_ContinueWhenOnline = 1
	SpellResumeType_ContinueByTime     = 2
)

// SpellEffectStyle
const (
	SpellEffectStyle_None     = 0
	SpellEffectStyle_Positive = 1
	SpellEffectStyle_Negative = 2
	SpellEffectStyle_All      = 3
)

// SpellInterruptBy
const (
	SpellInterruptBy_None    = 0
	SpellInterruptBy_Move    = 1
	SpellInterruptBy_Injured = 2
)

// SpellEffectType
const (
	SpellEffectType_None        = 0
	SpellEffectType_Attack      = 1
	SpellEffectType_AuraAttack  = 2
	SpellEffectType_ChangeProp  = 3
	SpellEffectType_ChangeSpeed = 4
	SpellEffectType_Invincible  = 5
	SpellEffectType_Invisible   = 6
	SpellEffectType_Fetter      = 7
	SpellEffectType_Stun        = 8

	SpellEffectType_AuraObject = 300

	SpellEffectType_Teleport = 500
	SpellEffectType_Mine     = 501
)

// SpellTargetType
const (
	SpellTargetType_Any        = 0
	SpellTargetType_Friend     = 1
	SpellTargetType_Enemy      = 2
	SpellTargetType_TeamMember = 3
	SpellTargetType_Count      = 4
)

// SpellSelectType
const (
	SpellSelectType_None             = 0
	SpellSelectType_Self             = 1
	SpellSelectType_Friend           = 2
	SpellSelectType_Enemy            = 3
	SpellSelectType_Friends          = 4
	SpellSelectType_Enemies          = 5
	SpellSelectType_SelfOrFriend     = 6
	SpellSelectType_FocusFriends     = 7
	SpellSelectType_FocusEnemies     = 8
	SpellSelectType_TeamMember       = 9
	SpellSelectType_TeamMembers      = 10
	SpellSelectType_SelfOrTeamMember = 11
	SpellSelectType_FocusTeamMembers = 12
	SpellSelectType_Reference        = 13
	SpellSelectType_Count            = 14
)

// SpellSelectMode
const (
	SpellSelectMode_Circle = 0
	SpellSelectMode_Sector = 1
	SpellSelectMode_Rect   = 2
	SpellSelectMode_Count  = 3
)

// SpellPassiveMode
const (
	SpellPassiveMode_Status = 0
	SpellPassiveMode_Event  = 1
	SpellPassiveMode_Count  = 2
)

// SpellPassiveBy
const (
	SpellPassiveBy_StatusNone    = 0
	SpellPassiveBy_StatusHPValue = 1
	SpellPassiveBy_StatusHPRate  = 2
	SpellPassiveBy_StatusMax     = 3

	SpellPassiveBy_EventHit   = 0
	SpellPassiveBy_EventHitBy = 1
	SpellPassiveBy_EventMax   = 2
)

type SpellInfo_Flags struct {
	IsExclusive          bool `json:"isExclusive,omitempty"`
	IsPassive            bool `json:"isPassive,omitempty"`
	IsAppearance         bool `json:"isAppearance,omitempty"`
	IsSync2Client        bool `json:"isSync2Client,omitempty"`
	IsSync2AllClient     bool `json:"isSync2AllClient,omitempty"`
	IsTargetDeadable     bool `json:"isTargetDeadable,omitempty"`
	IsCheckCastDist      bool `json:"isCheckCastDist,omitempty"`
	IsIgnoreOutOfControl bool `json:"isIgnoreOutOfControl,omitempty"`
}

type SpellInfo struct {
	SpellID              uint32          `json:"spellID,omitempty" rule:"required"`
	SpellIcon            string          `json:"spellIcon,omitempty" rule:"required"`
	SpellFlags           SpellInfo_Flags `json:"spellFlags,omitempty" rule:"required"`
	SpellBuffType        uint8           `json:"spellBuffType,omitempty" rule:"required"`
	SpellCooldownType    uint8           `json:"spellCooldownType,omitempty" rule:"required"`
	SpellTargetType      uint8           `json:"spellTargetType,omitempty" rule:"required"`
	SpellPassiveMode     uint8           `json:"spellPassiveMode,omitempty" rule:"required"`
	SpellPassiveBy       uint8           `json:"spellPassiveBy,omitempty" rule:"required"`
	SpellPassiveArgs     []float32       `json:"spellPassiveArgs,omitempty" rule:"required"`
	SpellPassiveChance   float32         `json:"spellPassiveChance,omitempty" rule:"required"`
	SpellLimitCareer     uint32          `json:"spellLimitCareer,omitempty" rule:"required"`
	SpellLimitMapType    uint32          `json:"spellLimitMapType,omitempty" rule:"required"`
	SpellLimitMapID      uint32          `json:"spellLimitMapID,omitempty" rule:"required"`
	SpellLimitScriptID   uint32          `json:"spellLimitScriptID,omitempty" rule:"required"`
	SpellLimitScriptArgs string          `json:"spellLimitScriptArgs,omitempty" rule:"required"`
	SpellInterruptBys    uint32          `json:"spellInterruptBys,omitempty" rule:"required"`
	SpellStageTime       []uint32        `json:"spellStageTime,omitempty" rule:"required"`
}

func (*SpellInfo) GetTableName() string {
	return "spell_info"
}
func (*SpellInfo) GetTableKeyName() string {
	return "spellID"
}
func (obj *SpellInfo) GetTableKeyValue() uint {
	return uint(obj.SpellID)
}

type SpellLevelInfo struct {
	SpellLevelID     uint32  `json:"spellLevelID,omitempty" rule:"required"`
	SpellID          uint32  `json:"spellID,omitempty" rule:"required"`
	SpellLimitLevel  uint32  `json:"spellLimitLevel,omitempty" rule:"required"`
	SpellCastDistMin float32 `json:"spellCastDistMin,omitempty" rule:"required"`
	SpellCastDistMax float32 `json:"spellCastDistMax,omitempty" rule:"required"`
	SpellEvalScore   uint32  `json:"spellEvalScore,omitempty" rule:"required"`
	SpellCostHP      uint32  `json:"spellCostHP,omitempty" rule:"required"`
	SpellCostHPRate  float32 `json:"spellCostHPRate,omitempty" rule:"required"`
	SpellCostMP      uint32  `json:"spellCostMP,omitempty" rule:"required"`
	SpellCostMPRate  float32 `json:"spellCostMPRate,omitempty" rule:"required"`
	SpellCDTime      uint32  `json:"spellCDTime,omitempty" rule:"required"`
}

func (*SpellLevelInfo) GetTableName() string {
	return "spell_level_info"
}
func (*SpellLevelInfo) GetTableKeyName() string {
	return "spellLevelID"
}
func (obj *SpellLevelInfo) GetTableKeyValue() uint {
	return uint(obj.SpellLevelID)
}

type SpellLevelEffectInfo_Flags struct {
	IsResumable          bool `json:"isResumable,omitempty"`
	IsDeadResumable      bool `json:"isDeadResumable,omitempty"`
	IsSync2Client        bool `json:"isSync2Client,omitempty"`
	IsSync2AllClient     bool `json:"isSync2AllClient,omitempty"`
	IsTargetDeadable     bool `json:"isTargetDeadable,omitempty"`
	IsClientSelectTarget bool `json:"isClientSelectTarget,omitempty"`
}

type SpellLevelEffectInfo struct {
	SpellLevelEffectID   uint32                     `json:"spellLevelEffectID,omitempty" rule:"required"`
	SpellID              uint32                     `json:"spellID,omitempty" rule:"required"`
	SpellLevelID         uint32                     `json:"spellLevelID,omitempty" rule:"required"`
	SpellEffectFlags     SpellLevelEffectInfo_Flags `json:"spellEffectFlags,omitempty" rule:"required"`
	SpellStageTrigger    uint8                      `json:"spellStageTrigger,omitempty" rule:"required"`
	SpellDelayTrigger    uint32                     `json:"spellDelayTrigger,omitempty" rule:"required"`
	SpellSelectType      uint8                      `json:"spellSelectType,omitempty" rule:"required"`
	SpellSelectMode      uint8                      `json:"spellSelectMode,omitempty" rule:"required"`
	SpellSelectArgs      []float32                  `json:"spellSelectArgs,omitempty" rule:"required"`
	SpellSelectNumber    uint32                     `json:"spellSelectNumber,omitempty" rule:"required"`
	SpellDelayEffective  uint32                     `json:"spellDelayEffective,omitempty" rule:"required"`
	SpellEffectiveChance float32                    `json:"spellEffectiveChance,omitempty" rule:"required"`
	SpellEffectStyle     uint32                     `json:"spellEffectStyle,omitempty" rule:"required"`
	SpellEffectType      uint32                     `json:"spellEffectType,omitempty" rule:"required"`
	SpellEffectArgs      string                     `json:"spellEffectArgs,omitempty" rule:"required"`
}

func (*SpellLevelEffectInfo) GetTableName() string {
	return "spell_level_effect_info"
}
func (*SpellLevelEffectInfo) GetTableKeyName() string {
	return "spellLevelEffectID"
}
func (obj *SpellLevelEffectInfo) GetTableKeyValue() uint {
	return uint(obj.SpellLevelEffectID)
}

// AuraSelectType
const (
	AuraSelectType_Friend     = 0
	AuraSelectType_Enemy      = 1
	AuraSelectType_Player     = 2
	AuraSelectType_Creature   = 3
	AuraSelectType_TeamMember = 4
	AuraSelectType_Count      = 5
)
