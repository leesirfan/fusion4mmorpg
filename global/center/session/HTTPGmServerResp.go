package session

import (
	"fmt"
	"net/http"
	"reflect"

	"test.com/common"
	"test.com/fusion"
)

func HandleReloadGameServerInfos(w http.ResponseWriter, r *http.Request) {
	var gsInfo *common.GameServerInfo
	gsInfoVals, err := fusion.LoadTableFromDB(common.GlobalDB, reflect.TypeOf(gsInfo), "")
	if err != nil {
		w.Write([]byte(fmt.Sprintf("Opps, %s.", err)))
		return
	}
	gsInfos := gsInfoVals.Interface().([]*common.GameServerInfo)
	fusion.MainService.AsyncBlockInvoke(func() {
		InstGameServerMgr.ReloadGameServerInfos(gsInfos)
	})
	w.Write([]byte("All Is OK!"))
}
