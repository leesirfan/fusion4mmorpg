module test.com

go 1.13

require (
	github.com/NYTimes/gziphandler v1.1.1
	github.com/go-redis/redis v6.15.7+incompatible
	github.com/go-sql-driver/mysql v1.5.0
	github.com/onsi/ginkgo v1.12.0 // indirect
	github.com/onsi/gomega v1.9.0 // indirect
	github.com/panjf2000/ants/v2 v2.3.0
)
