package main

import (
	"database/sql"
	"reflect"

	"test.com/fusion"
	"test.com/worlddb"
)

func getAllScriptable(db *sql.DB) ([]*worlddb.Scriptable, error) {
	var scriptable *worlddb.Scriptable
	scriptableVals, err := fusion.
		LoadJsontableFromDB(worldDB, reflect.TypeOf(scriptable), "")
	if err != nil {
		return nil, err
	}
	return scriptableVals.Interface().([]*worlddb.Scriptable), nil
}
