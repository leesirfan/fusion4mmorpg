#include "preHeader.h"
#include "GateServerMaster.h"

int main(int argc, char *argv[])
{
	GateServerMaster::newInstance();
	sGateServerMaster.InitSingleton();

	if (sGateServerMaster.Initialize(argc, argv) == 0) {
		sGateServerMaster.Run(argc, argv);
	}

	sGateServerMaster.FinishSingleton();
	GateServerMaster::deleteInstance();

	printf("Gate Server shutdown gracefully!\n");

	return 0;
}
