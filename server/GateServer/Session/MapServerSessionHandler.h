#pragma once

#include "Singleton.h"
#include "MessageHandler.h"
#include "protocol/InternalProtocol.h"

class MapServerSession;

class MapServerSessionHandler :
	public MessageHandler<MapServerSessionHandler, MapServerSession, GateServer2MapServer::GATE2MAP_MESSAGE_COUNT>,
	public Singleton<MapServerSessionHandler>
{
public:
	MapServerSessionHandler();
	virtual ~MapServerSessionHandler();
private:
	int HandleRegisterResp(MapServerSession *pSession, INetPacket &pck);
	int HandlePushPacketToAllClient(MapServerSession *pSession, INetPacket &pck);
};

#define sMapServerSessionHandler (*MapServerSessionHandler::instance())
