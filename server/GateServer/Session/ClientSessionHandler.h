#pragma once

#include "Singleton.h"
#include "MessageHandler.h"
#include "protocol/OpCode.h"

class ClientSession;

class ClientSessionHandler :
	public MessageHandler<ClientSessionHandler, ClientSession, GAME_OPCODE::CSMSG_COUNT>,
	public Singleton<ClientSessionHandler>
{
public:
	ClientSessionHandler();
	virtual ~ClientSessionHandler();
private:
	int HandlePing(ClientSession *pSession, INetPacket &pck);
	int HandleMmorpgLogin(ClientSession *pSession, INetPacket &pck);
	int HandleMmorpgLogout(ClientSession *pSession, INetPacket &pck);
};

#define sClientSessionHandler (*ClientSessionHandler::instance())
