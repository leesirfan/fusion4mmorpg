#include "preHeader.h"
#include "MapServerMgr.h"
#include "GameServerSession.h"

MapServerMgr::MapServerMgr()
{
}

MapServerMgr::~MapServerMgr()
{
	for (const auto& pair : m_MapServerMap) {
		delete pair.second;
	}
}

void MapServerMgr::CheckConnections()
{
	if (sGameServerSession.GetServerId() != 0) {
		auto itr = m_MapServerMap.begin();
		while (itr != m_MapServerMap.end()) {
			itr++->second->CheckConnection();
		}
	}
}

void MapServerMgr::AddMapServer(MapServerSession* pSession)
{
	m_MapServerMap.emplace(pSession->GetMSSN(), pSession);
}

void MapServerMgr::RemoveMapServer(MapServerSession* pSession)
{
	m_MapServerMap.erase(pSession->GetMSSN());
}

MapServerSession* MapServerMgr::GetMapServer(uint32 msSN) const
{
	auto itr = m_MapServerMap.find(msSN);
	return itr != m_MapServerMap.end() ? itr->second : NULL;
}

void MapServerMgr::ClearAllMapServers()
{
	for (auto& pair : m_MapServerMap) {
		pair.second->Cancel();
	}
}
