#pragma once

#include "Singleton.h"
#include "Session/ClientSession.h"
#include "Session/MapServerSession.h"

class TransMgr : public Singleton<TransMgr>
{
public:
	TransMgr();
	virtual ~TransMgr();

	void RunGC();

	void AddClient(ClientSession* pSession);
	void RemoveClient(ClientSession* pSession);
	ClientSession* GetClient(uint32 sn);

	void AddMapServer(MapServerSession* pSession);
	void RemoveMapServer(MapServerSession* pSession);
	MapServerSession* GetMapServer(uint32 msSN);

private:
	std::shared_mutex m_clientMutex;
	std::unordered_map<uint32, ClientSession*> m_clientMap;

	std::shared_mutex m_mapServerMutex;
	std::unordered_map<uint32, MapServerSession*> m_mapServerMap;

	std::queue<std::pair<std::shared_ptr<Connection>, time_t>> m_recycleBin;
};

#define sTransMgr (*TransMgr::instance())
