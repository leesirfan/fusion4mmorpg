enum ATTRARITHTYPE
{
	BASE,
	SCALE,
	FINAL,
	COUNT
}

enum ATTRTYPE
{
	NONE,

	HIT_POINT,
	MAGIC_POINT,

	ATTACK_VALUE,
	DEFENSE_VALUE,

	HIT_CHANCE,
	DODGE_CHANCE,
	CRITIHIT_CHANCE,
	CRITIHIT_CHANCE_RESIST,
	CRITIHIT_INTENSITY,
	CRITIHIT_INTENSITY_RESIST,

	COUNT
}

table PlayerBase
{
	required uint32 level;
	required uint64 lvUpExp;
	required double recoveryHPRate;
	required double recoveryHPValue;
	required double recoveryMPRate;
	required double recoveryMPValue;
	(key=level, tblname=player_base)
}

table PlayerAttribute
{
	required uint32 Id;
	required uint32 career;
	required uint32 level;
	required double[] attrs;
	required double damageFactor;
	(key=Id, tblname=player_attribute)
}

table CreatureAttribute
{
	required uint64 Id;
	required uint32 type;
	required uint32 level;
	required uint32 round;
	required double[] attrs;
	required double damageFactor;
	(key=Id, tblname=creature_attribute)
}