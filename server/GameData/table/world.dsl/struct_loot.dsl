table LootSet
{
	required uint32 lsID;
	required string lsName;
	(key=lsID, tblname=loot_set)
}

table LootSetGroup
{
	required uint32 lsgID;
	required uint32 lsID;
	required string lsgName;
	required float lsgOdds;
	required uint32 lsgMinItemTimes;
	required uint32 lsgMaxItemTimes;
	required uint32 lsgMinChequeTimes;
	required uint32 lsgMaxChequeTimes;
	(key=lsgID, tblname=loot_set_group)
}

table LootSetGroupItem
{
	struct Flags {
		bool bindToPicker;
	}
	required uint32 lsgiID;
	required uint32 lsID;
	required uint32 lsgID;
	required Flags lsgiFlags;
	required uint32 lsgiWeight;
	required uint32 lsgiItemTypeID;
	required uint32 lsgiMinCount;
	required uint32 lsgiMaxCount;
	required uint32 lsgiMaxTimes;
	required uint32 lsgiLimitQuest;
	required uint32 lsgiLimitCareer;
	required uint32 lsgiLimitGender;
	(key=lsgiID, tblname=loot_set_group_item)
}

table LootSetGroupCheque
{
	required uint32 lsgcID;
	required uint32 lsID;
	required uint32 lsgID;
	required uint32 lsgcWeight;
	required uint8 lsgcChequeType;
	required uint64 lsgcMinValue;
	required uint64 lsgcMaxValue;
	required uint32 lsgcMaxTimes;
	(key=lsgcID, tblname=loot_set_group_cheque)
}