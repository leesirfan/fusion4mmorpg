enum QuestNavIdx
{
	Publisher = -1,
	Verifier = -2,
	Guider = -3,
}

enum QuestClassType
{
	None,
	MainLine,
	BranchLine,
}

enum QuestRepeatType
{
	None,
	Daily,
	Weekly,
	Monthly,
	Forever,
	Count
}

enum QuestWhenType
{
	Accept,
	Finish,
	Failed,
	Cancel,
	Submit,
	Count
}

enum QuestObjType
{
	None,
	Guide,
	NPC,
	SObj,
	NPCPt,
	SObjPt,
	Count
}

struct QuestObjInst
{
	uint32 objType;
	uint32 objID;
}

struct QuestScript
{
	uint32 scriptID;
	string scriptArgs;
}

struct QuestCheque
{
	uint8 chequeType;
	uint64 chequeValue;
}

struct QuestItem
{
	uint32 itemTypeID;
	uint32 itemCount;
	uint32 onlyCareer;
	uint32 onlyGender;
}

struct QuestChequeReq : QuestCheque
{
	bool isCost;
	bool isRefund;
}

struct QuestItemReq : QuestItem
{
	bool isDestroy;
	bool isRefund;
	bool isBinding;
}

struct QuestQuestionReq
{
	uint32 questionId;
	uint8 optionId;
}

struct QuestChequeInit : QuestCheque
{
	bool isRetrieve;
}

struct QuestItemInit : QuestItem
{
	bool isBinding;
	bool isRetrieve;
}

struct QuestChequeReward : QuestCheque
{
	bool isFixed;
}

struct QuestItemReward : QuestItem
{
	bool isBinding;
}

enum QuestConditionType
{
	None,
	TalkNPC,
	KillCreature,
	HaveCheque,
	HaveItem,
	UseItem,
	PlayStory,
	Count
}

struct QuestCondition
{
	uint32 conditionType;
	uint32[] conditionIds;
	uint64 conditionNum;
	uint32[] conditionArgs;
	bool[] conditionFlags;
	QuestObjInst forNavInfo;
}

table QuestPrototype
{
	struct Flags {
		bool isWatchStatus;
		bool isAutoAccept;
		bool isAutoSubmit;
		bool isStoryMode;
		bool isEnterSceneAeap;
		bool isLeaveSceneAeap;
		bool isCantArchive;
		bool isCantCancel;
		bool isAnyReqQuest;
		bool isAnyCondition;
		bool isRemoveFailed;
	}

	required uint32 questTypeID;
	required uint32 questClass;
	required Flags questFlags;
	required QuestObjInst questPublisher;
	required QuestObjInst questVerifier;
	required QuestObjInst questGuider;
	required uint32 questTimeMax;

	required uint32 questRepeatType;
	required uint32 questRepeatMax;

	required uint32 questReqMinLv;
	required uint32 questReqMaxLv;
	required uint32 questReqCareer;
	required uint32 questReqGender;
	required uint32[] questReqQuests;
	required QuestChequeReq[] questReqCheques;
	required QuestItemReq[] questReqItems;
	required QuestQuestionReq questReqQuestion;
	required QuestScript questReqExtra;

	required QuestChequeInit[] questInitCheques;
	required QuestItemInit[] questInitItems;
	required QuestScript questInitExtra;

	required QuestChequeReward[] questRewardCheques;
	required QuestItemReward[] questRewardItems;
	required QuestScript questRewardExtra;

	required QuestScript[] questScripts;
	required QuestCondition[] questConditions;

	(key=questTypeID, tblname=quest_prototype)
}

table QuestCreatureVisible
{
	required uint64 Id;
	required uint32 questTypeID;
	required int8 questWhenType;
	required bool isVisible;
	required uint32[] spawnIDs;
	(key=Id, tblname=quest_creature_visible)
}