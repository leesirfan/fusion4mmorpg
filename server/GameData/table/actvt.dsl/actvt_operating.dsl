#include "table/world/struct_base.h"

enum OperatingActivityType
{
	None,
	ChargeSelfSelect,  // 充值自选活动
	AccumulativeCharge,  // 累计充值活动
}

table OperatingActivity
{
	required uint32 nId;
	required string strName;
	required string strDesc;
	required uint32 nUIType;
	required uint32 nType;
	required uint32 nTimes;
	required uint32 nPreType;
	required uint32 nPreTimes;
	required uint32 nShowReqVipLevel;
	required uint32 nShowReqLevel;
	required uint32 nPlayReqVipLevel;
	required uint32 nPlayReqLevel;
	required datetime nActvtReferTime;
	required datetime nActvtStopTime;
	required datetime nShowStartTime;
	required datetime nShowEndTime;
	required datetime nPlayStartTime;
	required datetime nPlayEndTime;
	required time nDailyStartTime;
	required time nDailyEndTime;
	required string strOpenServers;
	required string strNotOpenServers;
	required string strActivityParams;
	required string strClientParams;
	(tblname=operating_activities)
}

table OperatingActivityUIType
{
	required uint32 nId;
	required uint32 nUIType;
	required string strOpenServers;
	required string strNotOpenServers;
	required string strClientParams;
	(tblname=operating_activity_uitypes)
}

enum OperatingPlayType
{
	Invalid = -1,
	LoginDay = 0,  // 登陆?天
	LevelReach,  // 达到?级
	VipLevelReach,  // 达到VIP?以上
	FightReach,  // 战力达到?以上
	Count
};

struct OperatingChargeSelfSelect
{
	uint32 gradVal;
	uint32 maxTimes;
	struct RewardData {
		FItemInfo[] rewardItems;
	}
	RewardData[] rewardDataList;
}

struct OperatingAccumulativeCharge
{
	struct RewardData {
		uint32 gradVal;
		FItemInfo[] rewardItems;
	}
	RewardData[] rewardDataList;
}