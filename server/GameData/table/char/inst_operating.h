#pragma once

struct OperatingActivityData
{
	OperatingActivityData();

	uint32 playerId;
	uint32 actType;
	uint32 actTimes;
	std::vector<uint32> saveData;
	std::string userData;
};
