#include "jsontable/table_helper.h"
#include "inst_mail.h"

template<> void LoadFromStream(MAIL_RQST_TYPE &entity, std::istream &stream)
{
	StreamHelper::EnumFromStream(entity, stream);
}

template<> void SaveToStream(const MAIL_RQST_TYPE &entity, std::ostream &stream)
{
	StreamHelper::EnumToStream(entity, stream);
}

template<> void LoadFromText(MAIL_RQST_TYPE &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const MAIL_RQST_TYPE &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(MAIL_RQST_TYPE &entity, const rapidjson::Value &value)
{
	EnumFromJson(entity, value);
}

template<> void JsonHelper::BlockToJson(const MAIL_RQST_TYPE &entity, rapidjson::Value &value)
{
	EnumToJson(entity, value);
}

template<> void LoadFromStream(MAIL_TYPE &entity, std::istream &stream)
{
	StreamHelper::EnumFromStream(entity, stream);
}

template<> void SaveToStream(const MAIL_TYPE &entity, std::ostream &stream)
{
	StreamHelper::EnumToStream(entity, stream);
}

template<> void LoadFromText(MAIL_TYPE &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const MAIL_TYPE &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(MAIL_TYPE &entity, const rapidjson::Value &value)
{
	EnumFromJson(entity, value);
}

template<> void JsonHelper::BlockToJson(const MAIL_TYPE &entity, rapidjson::Value &value)
{
	EnumToJson(entity, value);
}

template<> void LoadFromStream(MailFlag &entity, std::istream &stream)
{
	StreamHelper::EnumFromStream(entity, stream);
}

template<> void SaveToStream(const MailFlag &entity, std::ostream &stream)
{
	StreamHelper::EnumToStream(entity, stream);
}

template<> void LoadFromText(MailFlag &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const MailFlag &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(MailFlag &entity, const rapidjson::Value &value)
{
	EnumFromJson(entity, value);
}

template<> void JsonHelper::BlockToJson(const MailFlag &entity, rapidjson::Value &value)
{
	EnumToJson(entity, value);
}

template<> const char *GetTableName<InstMailAttachment>()
{
	return "inst_mail";
}

template<> const char *GetTableKeyName<InstMailAttachment>()
{
	return "mailID";
}

template<> uint64 GetTableKeyValue(const InstMailAttachment &entity)
{
	return (uint64)entity.mailID;
}

template<> void SetTableKeyValue(InstMailAttachment &entity, uint64 key)
{
	entity.mailID = (uint32)key;
}

template<> const char *GetTableFieldNameByIndex<InstMailAttachment>(size_t index)
{
	switch (index)
	{
		case 0: return "mailID";
		case 1: return "mailCheques";
		case 2: return "mailItems";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<InstMailAttachment>(const char *name)
{
	if (strcmp(name, "mailID") == 0) return 0;
	if (strcmp(name, "mailCheques") == 0) return 1;
	if (strcmp(name, "mailItems") == 0) return 2;
	return -1;
}

template<> size_t GetTableFieldNumber<InstMailAttachment>()
{
	return 3;
}

template<> std::string GetTableFieldValue(const InstMailAttachment &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.mailID);
		case 1: return JsonHelper::SequenceToJsonText(entity.mailCheques);
		case 2: return JsonHelper::SequenceToJsonText(entity.mailItems);
	}
	return "";
}

template<> void SetTableFieldValue(InstMailAttachment &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.mailID, value);
		case 1: return JsonHelper::SequenceFromJsonText(entity.mailCheques, value);
		case 2: return JsonHelper::SequenceFromJsonText(entity.mailItems, value);
	}
}

template<> void LoadFromStream(InstMailAttachment &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.mailID, stream);
	StreamHelper::SequenceFromStream(entity.mailCheques, stream);
	StreamHelper::SequenceFromStream(entity.mailItems, stream);
}

template<> void SaveToStream(const InstMailAttachment &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.mailID, stream);
	StreamHelper::SequenceToStream(entity.mailCheques, stream);
	StreamHelper::SequenceToStream(entity.mailItems, stream);
}

template<> void LoadFromText(InstMailAttachment &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const InstMailAttachment &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(InstMailAttachment &entity, const rapidjson::Value &value)
{
	FromJson(entity.mailID, value, "mailID");
	SequenceFromJson(entity.mailCheques, value, "mailCheques");
	SequenceFromJson(entity.mailItems, value, "mailItems");
}

template<> void JsonHelper::BlockToJson(const InstMailAttachment &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.mailID, value, "mailID");
	SequenceToJson(entity.mailCheques, value, "mailCheques");
	SequenceToJson(entity.mailItems, value, "mailItems");
}

template<> const char *GetTableName<inst_mail>()
{
	return "inst_mail";
}

template<> const char *GetTableKeyName<inst_mail>()
{
	return "mailID";
}

template<> uint64 GetTableKeyValue(const inst_mail &entity)
{
	return (uint64)entity.mailID;
}

template<> void SetTableKeyValue(inst_mail &entity, uint64 key)
{
	entity.mailID = (uint32)key;
}

template<> const char *GetTableFieldNameByIndex<inst_mail>(size_t index)
{
	switch (index)
	{
		case 0: return "mailID";
		case 1: return "mailType";
		case 2: return "mailFlags";
		case 3: return "mailSender";
		case 4: return "mailSenderName";
		case 5: return "mailReceiver";
		case 6: return "mailDeliverTime";
		case 7: return "mailExpireTime";
		case 8: return "mailSubject";
		case 9: return "mailBody";
		case 10: return "mailCheques";
		case 11: return "mailItems";
		case 12: return "isGetAttachment";
		case 13: return "isViewDetail";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<inst_mail>(const char *name)
{
	if (strcmp(name, "mailID") == 0) return 0;
	if (strcmp(name, "mailType") == 0) return 1;
	if (strcmp(name, "mailFlags") == 0) return 2;
	if (strcmp(name, "mailSender") == 0) return 3;
	if (strcmp(name, "mailSenderName") == 0) return 4;
	if (strcmp(name, "mailReceiver") == 0) return 5;
	if (strcmp(name, "mailDeliverTime") == 0) return 6;
	if (strcmp(name, "mailExpireTime") == 0) return 7;
	if (strcmp(name, "mailSubject") == 0) return 8;
	if (strcmp(name, "mailBody") == 0) return 9;
	if (strcmp(name, "mailCheques") == 0) return 10;
	if (strcmp(name, "mailItems") == 0) return 11;
	if (strcmp(name, "isGetAttachment") == 0) return 12;
	if (strcmp(name, "isViewDetail") == 0) return 13;
	return -1;
}

template<> size_t GetTableFieldNumber<inst_mail>()
{
	return 14;
}

template<> std::string GetTableFieldValue(const inst_mail &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.mailID);
		case 1: return StringHelper::ToString(entity.mailType);
		case 2: return StringHelper::ToString(entity.mailFlags);
		case 3: return StringHelper::ToString(entity.mailSender);
		case 4: return StringHelper::ToString(entity.mailSenderName);
		case 5: return StringHelper::ToString(entity.mailReceiver);
		case 6: return StringHelper::ToString(entity.mailDeliverTime);
		case 7: return StringHelper::ToString(entity.mailExpireTime);
		case 8: return StringHelper::ToString(entity.mailSubject);
		case 9: return StringHelper::ToString(entity.mailBody);
		case 10: return JsonHelper::SequenceToJsonText(entity.mailCheques);
		case 11: return JsonHelper::SequenceToJsonText(entity.mailItems);
		case 12: return StringHelper::ToString(entity.isGetAttachment);
		case 13: return StringHelper::ToString(entity.isViewDetail);
	}
	return "";
}

template<> void SetTableFieldValue(inst_mail &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.mailID, value);
		case 1: return StringHelper::FromString(entity.mailType, value);
		case 2: return StringHelper::FromString(entity.mailFlags, value);
		case 3: return StringHelper::FromString(entity.mailSender, value);
		case 4: return StringHelper::FromString(entity.mailSenderName, value);
		case 5: return StringHelper::FromString(entity.mailReceiver, value);
		case 6: return StringHelper::FromString(entity.mailDeliverTime, value);
		case 7: return StringHelper::FromString(entity.mailExpireTime, value);
		case 8: return StringHelper::FromString(entity.mailSubject, value);
		case 9: return StringHelper::FromString(entity.mailBody, value);
		case 10: return JsonHelper::SequenceFromJsonText(entity.mailCheques, value);
		case 11: return JsonHelper::SequenceFromJsonText(entity.mailItems, value);
		case 12: return StringHelper::FromString(entity.isGetAttachment, value);
		case 13: return StringHelper::FromString(entity.isViewDetail, value);
	}
}

template<> void LoadFromStream(inst_mail &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.mailID, stream);
	StreamHelper::FromStream(entity.mailType, stream);
	StreamHelper::FromStream(entity.mailFlags, stream);
	StreamHelper::FromStream(entity.mailSender, stream);
	StreamHelper::FromStream(entity.mailSenderName, stream);
	StreamHelper::FromStream(entity.mailReceiver, stream);
	StreamHelper::FromStream(entity.mailDeliverTime, stream);
	StreamHelper::FromStream(entity.mailExpireTime, stream);
	StreamHelper::FromStream(entity.mailSubject, stream);
	StreamHelper::FromStream(entity.mailBody, stream);
	StreamHelper::SequenceFromStream(entity.mailCheques, stream);
	StreamHelper::SequenceFromStream(entity.mailItems, stream);
	StreamHelper::FromStream(entity.isGetAttachment, stream);
	StreamHelper::FromStream(entity.isViewDetail, stream);
}

template<> void SaveToStream(const inst_mail &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.mailID, stream);
	StreamHelper::ToStream(entity.mailType, stream);
	StreamHelper::ToStream(entity.mailFlags, stream);
	StreamHelper::ToStream(entity.mailSender, stream);
	StreamHelper::ToStream(entity.mailSenderName, stream);
	StreamHelper::ToStream(entity.mailReceiver, stream);
	StreamHelper::ToStream(entity.mailDeliverTime, stream);
	StreamHelper::ToStream(entity.mailExpireTime, stream);
	StreamHelper::ToStream(entity.mailSubject, stream);
	StreamHelper::ToStream(entity.mailBody, stream);
	StreamHelper::SequenceToStream(entity.mailCheques, stream);
	StreamHelper::SequenceToStream(entity.mailItems, stream);
	StreamHelper::ToStream(entity.isGetAttachment, stream);
	StreamHelper::ToStream(entity.isViewDetail, stream);
}

template<> void LoadFromText(inst_mail &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const inst_mail &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(inst_mail &entity, const rapidjson::Value &value)
{
	FromJson(entity.mailID, value, "mailID");
	FromJson(entity.mailType, value, "mailType");
	FromJson(entity.mailFlags, value, "mailFlags");
	FromJson(entity.mailSender, value, "mailSender");
	FromJson(entity.mailSenderName, value, "mailSenderName");
	FromJson(entity.mailReceiver, value, "mailReceiver");
	FromJson(entity.mailDeliverTime, value, "mailDeliverTime");
	FromJson(entity.mailExpireTime, value, "mailExpireTime");
	FromJson(entity.mailSubject, value, "mailSubject");
	FromJson(entity.mailBody, value, "mailBody");
	SequenceFromJson(entity.mailCheques, value, "mailCheques");
	SequenceFromJson(entity.mailItems, value, "mailItems");
	FromJson(entity.isGetAttachment, value, "isGetAttachment");
	FromJson(entity.isViewDetail, value, "isViewDetail");
}

template<> void JsonHelper::BlockToJson(const inst_mail &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.mailID, value, "mailID");
	ToJson(entity.mailType, value, "mailType");
	ToJson(entity.mailFlags, value, "mailFlags");
	ToJson(entity.mailSender, value, "mailSender");
	ToJson(entity.mailSenderName, value, "mailSenderName");
	ToJson(entity.mailReceiver, value, "mailReceiver");
	ToJson(entity.mailDeliverTime, value, "mailDeliverTime");
	ToJson(entity.mailExpireTime, value, "mailExpireTime");
	ToJson(entity.mailSubject, value, "mailSubject");
	ToJson(entity.mailBody, value, "mailBody");
	SequenceToJson(entity.mailCheques, value, "mailCheques");
	SequenceToJson(entity.mailItems, value, "mailItems");
	ToJson(entity.isGetAttachment, value, "isGetAttachment");
	ToJson(entity.isViewDetail, value, "isViewDetail");
}
