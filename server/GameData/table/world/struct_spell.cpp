#include "jsontable/table_helper.h"
#include "struct_spell.h"

SpellInfo::Flags::Flags()
: isExclusive(false)
, isPassive(false)
, isAppearance(false)
, isSync2Client(false)
, isSync2AllClient(false)
, isTargetDeadable(false)
, isCheckCastDist(false)
, isIgnoreOutOfControl(false)
{
}

SpellInfo::SpellInfo()
: spellID(0)
, spellBuffType(0)
, spellCooldownType(0)
, spellTargetType(0)
, spellPassiveMode(0)
, spellPassiveBy(0)
, spellPassiveChance(.0f)
, spellLimitCareer(0)
, spellLimitMapType(0)
, spellLimitMapID(0)
, spellLimitScriptID(0)
, spellInterruptBys(0)
{
}

SpellLevelInfo::SpellLevelInfo()
: spellLevelID(0)
, spellID(0)
, spellLimitLevel(0)
, spellCastDistMin(.0f)
, spellCastDistMax(.0f)
, spellEvalScore(0)
, spellCostHP(0)
, spellCostHPRate(.0f)
, spellCostMP(0)
, spellCostMPRate(.0f)
, spellCDTime(0)
{
}

SpellLevelEffectInfo::Flags::Flags()
: isResumable(false)
, isDeadResumable(false)
, isSync2Client(false)
, isSync2AllClient(false)
, isTargetDeadable(false)
, isClientSelectTarget(false)
{
}

SpellLevelEffectInfo::SpellLevelEffectInfo()
: spellLevelEffectID(0)
, spellID(0)
, spellLevelID(0)
, spellStageTrigger(0)
, spellDelayTrigger(0)
, spellSelectType(0)
, spellSelectMode(0)
, spellSelectNumber(0)
, spellDelayEffective(0)
, spellEffectiveChance(.0f)
, spellEffectStyle(0)
, spellEffectType(0)
{
}
