#pragma once

struct ShopPrototype
{
	ShopPrototype();

	uint32 Id;
	uint32 shopType;
	uint32 itemTypeID;
	uint32 itemCount;
	uint32 itemFlags;
	uint32 currencyType;
	uint32 sellPrice;
	uint32 discountPrice;
	uint32 bundleNumMax;
	uint32 genderLimit;
	uint32 careerLimit;
	uint32 dailyBuyLimit;
	uint32 weeklyBuyLimit;
};

struct SpecialShopPrototype : ShopPrototype
{
	SpecialShopPrototype();

	uint64 itemUniqueKey;
	int64 itemBeginTime;
	int64 itemEndTime;
	uint32 serverBuyLimit;
	uint32 charBuyLimit;
	uint32 dailyServerBuyLimit;
	uint32 weeklyServerBuyLimit;
};
