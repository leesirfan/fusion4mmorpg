enum MAIL_RQST_TYPE
{
	COUNT_ALL,
	LIST_SOMES,
	GET_ATTACHMENT,
	VIEW_DETAIL,
	WRITE_ORDER,
	DELETE_ORDER,

	SEND_SINGLE,
	SEND_MULTI_SAME,
	SEND_MULTI_DIFFERENT,
}

enum MAIL_TYPE
{
	NORMAL,
	SYSTEM,
}

enum MailFlag
{
	SubjectArgs = 1 << 0,
	BodyArgs = 1 << 1,
}

table InstMailAttachment
{
	required uint32 mailID;
	required string[] mailCheques;
	required string[] mailItems;
	(key=mailID,tblname=inst_mail)
}

table inst_mail
{
	required uint32 mailID;
	required uint32 mailType;
	required uint32 mailFlags;
	required uint32 mailSender;
	required string mailSenderName;
	required uint32 mailReceiver;
	required int64 mailDeliverTime;
	required int64 mailExpireTime;
	required string mailSubject;
	required string mailBody;
	required string[] mailCheques;
	required string[] mailItems;
	required bool isGetAttachment;
	required bool isViewDetail;
	(key=mailID)
}