#pragma once

#include "table/world/struct_base.h"

enum class OperatingActivityType
{
	None,
	ChargeSelfSelect,  // 充值自选活动
	AccumulativeCharge,  // 累计充值活动
};

struct OperatingActivity
{
	OperatingActivity();

	uint32 nId;
	std::string strName;
	std::string strDesc;
	uint32 nUIType;
	uint32 nType;
	uint32 nTimes;
	uint32 nPreType;
	uint32 nPreTimes;
	uint32 nShowReqVipLevel;
	uint32 nShowReqLevel;
	uint32 nPlayReqVipLevel;
	uint32 nPlayReqLevel;
	int64 nActvtReferTime;
	int64 nActvtStopTime;
	int64 nShowStartTime;
	int64 nShowEndTime;
	int64 nPlayStartTime;
	int64 nPlayEndTime;
	int32 nDailyStartTime;
	int32 nDailyEndTime;
	std::string strOpenServers;
	std::string strNotOpenServers;
	std::string strActivityParams;
	std::string strClientParams;
};

struct OperatingActivityUIType
{
	OperatingActivityUIType();

	uint32 nId;
	uint32 nUIType;
	std::string strOpenServers;
	std::string strNotOpenServers;
	std::string strClientParams;
};

enum class OperatingPlayType
{
	Invalid = -1,
	LoginDay = 0,  // 登陆?天
	LevelReach,  // 达到?级
	VipLevelReach,  // 达到VIP?以上
	FightReach,  // 战力达到?以上
	Count,
};

struct OperatingChargeSelfSelect
{
	OperatingChargeSelfSelect();

	uint32 gradVal;
	uint32 maxTimes;
	struct RewardData {
		RewardData();
		std::vector<FItemInfo> rewardItems;
	};
	std::vector<RewardData> rewardDataList;
};

struct OperatingAccumulativeCharge
{
	OperatingAccumulativeCharge();

	struct RewardData {
		RewardData();
		uint32 gradVal;
		std::vector<FItemInfo> rewardItems;
	};
	std::vector<RewardData> rewardDataList;
};
