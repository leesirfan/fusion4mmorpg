#include "jsontable/table_helper.h"
#include "actvt_operating.h"

template<> void LoadFromStream(OperatingActivityType &entity, std::istream &stream)
{
	StreamHelper::EnumFromStream(entity, stream);
}

template<> void SaveToStream(const OperatingActivityType &entity, std::ostream &stream)
{
	StreamHelper::EnumToStream(entity, stream);
}

template<> void LoadFromText(OperatingActivityType &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const OperatingActivityType &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(OperatingActivityType &entity, const rapidjson::Value &value)
{
	EnumFromJson(entity, value);
}

template<> void JsonHelper::BlockToJson(const OperatingActivityType &entity, rapidjson::Value &value)
{
	EnumToJson(entity, value);
}

template<> const char *GetTableName<OperatingActivity>()
{
	return "operating_activities";
}

template<> const char *GetTableKeyName<OperatingActivity>()
{
	return "";
}

template<> uint64 GetTableKeyValue(const OperatingActivity &entity)
{
	return 0;
}

template<> void SetTableKeyValue(OperatingActivity &entity, uint64 key)
{
}

template<> const char *GetTableFieldNameByIndex<OperatingActivity>(size_t index)
{
	switch (index)
	{
		case 0: return "nId";
		case 1: return "strName";
		case 2: return "strDesc";
		case 3: return "nUIType";
		case 4: return "nType";
		case 5: return "nTimes";
		case 6: return "nPreType";
		case 7: return "nPreTimes";
		case 8: return "nShowReqVipLevel";
		case 9: return "nShowReqLevel";
		case 10: return "nPlayReqVipLevel";
		case 11: return "nPlayReqLevel";
		case 12: return "nActvtReferTime";
		case 13: return "nActvtStopTime";
		case 14: return "nShowStartTime";
		case 15: return "nShowEndTime";
		case 16: return "nPlayStartTime";
		case 17: return "nPlayEndTime";
		case 18: return "nDailyStartTime";
		case 19: return "nDailyEndTime";
		case 20: return "strOpenServers";
		case 21: return "strNotOpenServers";
		case 22: return "strActivityParams";
		case 23: return "strClientParams";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<OperatingActivity>(const char *name)
{
	if (strcmp(name, "nId") == 0) return 0;
	if (strcmp(name, "strName") == 0) return 1;
	if (strcmp(name, "strDesc") == 0) return 2;
	if (strcmp(name, "nUIType") == 0) return 3;
	if (strcmp(name, "nType") == 0) return 4;
	if (strcmp(name, "nTimes") == 0) return 5;
	if (strcmp(name, "nPreType") == 0) return 6;
	if (strcmp(name, "nPreTimes") == 0) return 7;
	if (strcmp(name, "nShowReqVipLevel") == 0) return 8;
	if (strcmp(name, "nShowReqLevel") == 0) return 9;
	if (strcmp(name, "nPlayReqVipLevel") == 0) return 10;
	if (strcmp(name, "nPlayReqLevel") == 0) return 11;
	if (strcmp(name, "nActvtReferTime") == 0) return 12;
	if (strcmp(name, "nActvtStopTime") == 0) return 13;
	if (strcmp(name, "nShowStartTime") == 0) return 14;
	if (strcmp(name, "nShowEndTime") == 0) return 15;
	if (strcmp(name, "nPlayStartTime") == 0) return 16;
	if (strcmp(name, "nPlayEndTime") == 0) return 17;
	if (strcmp(name, "nDailyStartTime") == 0) return 18;
	if (strcmp(name, "nDailyEndTime") == 0) return 19;
	if (strcmp(name, "strOpenServers") == 0) return 20;
	if (strcmp(name, "strNotOpenServers") == 0) return 21;
	if (strcmp(name, "strActivityParams") == 0) return 22;
	if (strcmp(name, "strClientParams") == 0) return 23;
	return -1;
}

template<> size_t GetTableFieldNumber<OperatingActivity>()
{
	return 24;
}

template<> std::string GetTableFieldValue(const OperatingActivity &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.nId);
		case 1: return StringHelper::ToString(entity.strName);
		case 2: return StringHelper::ToString(entity.strDesc);
		case 3: return StringHelper::ToString(entity.nUIType);
		case 4: return StringHelper::ToString(entity.nType);
		case 5: return StringHelper::ToString(entity.nTimes);
		case 6: return StringHelper::ToString(entity.nPreType);
		case 7: return StringHelper::ToString(entity.nPreTimes);
		case 8: return StringHelper::ToString(entity.nShowReqVipLevel);
		case 9: return StringHelper::ToString(entity.nShowReqLevel);
		case 10: return StringHelper::ToString(entity.nPlayReqVipLevel);
		case 11: return StringHelper::ToString(entity.nPlayReqLevel);
		case 12: return TableBase::ToStringDateTime(entity.nActvtReferTime);
		case 13: return TableBase::ToStringDateTime(entity.nActvtStopTime);
		case 14: return TableBase::ToStringDateTime(entity.nShowStartTime);
		case 15: return TableBase::ToStringDateTime(entity.nShowEndTime);
		case 16: return TableBase::ToStringDateTime(entity.nPlayStartTime);
		case 17: return TableBase::ToStringDateTime(entity.nPlayEndTime);
		case 18: return TableBase::ToStringTime(entity.nDailyStartTime);
		case 19: return TableBase::ToStringTime(entity.nDailyEndTime);
		case 20: return StringHelper::ToString(entity.strOpenServers);
		case 21: return StringHelper::ToString(entity.strNotOpenServers);
		case 22: return StringHelper::ToString(entity.strActivityParams);
		case 23: return StringHelper::ToString(entity.strClientParams);
	}
	return "";
}

template<> void SetTableFieldValue(OperatingActivity &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.nId, value);
		case 1: return StringHelper::FromString(entity.strName, value);
		case 2: return StringHelper::FromString(entity.strDesc, value);
		case 3: return StringHelper::FromString(entity.nUIType, value);
		case 4: return StringHelper::FromString(entity.nType, value);
		case 5: return StringHelper::FromString(entity.nTimes, value);
		case 6: return StringHelper::FromString(entity.nPreType, value);
		case 7: return StringHelper::FromString(entity.nPreTimes, value);
		case 8: return StringHelper::FromString(entity.nShowReqVipLevel, value);
		case 9: return StringHelper::FromString(entity.nShowReqLevel, value);
		case 10: return StringHelper::FromString(entity.nPlayReqVipLevel, value);
		case 11: return StringHelper::FromString(entity.nPlayReqLevel, value);
		case 12: return TableBase::FromStringDateTime(entity.nActvtReferTime, value);
		case 13: return TableBase::FromStringDateTime(entity.nActvtStopTime, value);
		case 14: return TableBase::FromStringDateTime(entity.nShowStartTime, value);
		case 15: return TableBase::FromStringDateTime(entity.nShowEndTime, value);
		case 16: return TableBase::FromStringDateTime(entity.nPlayStartTime, value);
		case 17: return TableBase::FromStringDateTime(entity.nPlayEndTime, value);
		case 18: return TableBase::FromStringTime(entity.nDailyStartTime, value);
		case 19: return TableBase::FromStringTime(entity.nDailyEndTime, value);
		case 20: return StringHelper::FromString(entity.strOpenServers, value);
		case 21: return StringHelper::FromString(entity.strNotOpenServers, value);
		case 22: return StringHelper::FromString(entity.strActivityParams, value);
		case 23: return StringHelper::FromString(entity.strClientParams, value);
	}
}

template<> void LoadFromStream(OperatingActivity &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.nId, stream);
	StreamHelper::FromStream(entity.strName, stream);
	StreamHelper::FromStream(entity.strDesc, stream);
	StreamHelper::FromStream(entity.nUIType, stream);
	StreamHelper::FromStream(entity.nType, stream);
	StreamHelper::FromStream(entity.nTimes, stream);
	StreamHelper::FromStream(entity.nPreType, stream);
	StreamHelper::FromStream(entity.nPreTimes, stream);
	StreamHelper::FromStream(entity.nShowReqVipLevel, stream);
	StreamHelper::FromStream(entity.nShowReqLevel, stream);
	StreamHelper::FromStream(entity.nPlayReqVipLevel, stream);
	StreamHelper::FromStream(entity.nPlayReqLevel, stream);
	StreamHelper::FromStream(entity.nActvtReferTime, stream);
	StreamHelper::FromStream(entity.nActvtStopTime, stream);
	StreamHelper::FromStream(entity.nShowStartTime, stream);
	StreamHelper::FromStream(entity.nShowEndTime, stream);
	StreamHelper::FromStream(entity.nPlayStartTime, stream);
	StreamHelper::FromStream(entity.nPlayEndTime, stream);
	StreamHelper::FromStream(entity.nDailyStartTime, stream);
	StreamHelper::FromStream(entity.nDailyEndTime, stream);
	StreamHelper::FromStream(entity.strOpenServers, stream);
	StreamHelper::FromStream(entity.strNotOpenServers, stream);
	StreamHelper::FromStream(entity.strActivityParams, stream);
	StreamHelper::FromStream(entity.strClientParams, stream);
}

template<> void SaveToStream(const OperatingActivity &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.nId, stream);
	StreamHelper::ToStream(entity.strName, stream);
	StreamHelper::ToStream(entity.strDesc, stream);
	StreamHelper::ToStream(entity.nUIType, stream);
	StreamHelper::ToStream(entity.nType, stream);
	StreamHelper::ToStream(entity.nTimes, stream);
	StreamHelper::ToStream(entity.nPreType, stream);
	StreamHelper::ToStream(entity.nPreTimes, stream);
	StreamHelper::ToStream(entity.nShowReqVipLevel, stream);
	StreamHelper::ToStream(entity.nShowReqLevel, stream);
	StreamHelper::ToStream(entity.nPlayReqVipLevel, stream);
	StreamHelper::ToStream(entity.nPlayReqLevel, stream);
	StreamHelper::ToStream(entity.nActvtReferTime, stream);
	StreamHelper::ToStream(entity.nActvtStopTime, stream);
	StreamHelper::ToStream(entity.nShowStartTime, stream);
	StreamHelper::ToStream(entity.nShowEndTime, stream);
	StreamHelper::ToStream(entity.nPlayStartTime, stream);
	StreamHelper::ToStream(entity.nPlayEndTime, stream);
	StreamHelper::ToStream(entity.nDailyStartTime, stream);
	StreamHelper::ToStream(entity.nDailyEndTime, stream);
	StreamHelper::ToStream(entity.strOpenServers, stream);
	StreamHelper::ToStream(entity.strNotOpenServers, stream);
	StreamHelper::ToStream(entity.strActivityParams, stream);
	StreamHelper::ToStream(entity.strClientParams, stream);
}

template<> void LoadFromText(OperatingActivity &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const OperatingActivity &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(OperatingActivity &entity, const rapidjson::Value &value)
{
	FromJson(entity.nId, value, "nId");
	FromJson(entity.strName, value, "strName");
	FromJson(entity.strDesc, value, "strDesc");
	FromJson(entity.nUIType, value, "nUIType");
	FromJson(entity.nType, value, "nType");
	FromJson(entity.nTimes, value, "nTimes");
	FromJson(entity.nPreType, value, "nPreType");
	FromJson(entity.nPreTimes, value, "nPreTimes");
	FromJson(entity.nShowReqVipLevel, value, "nShowReqVipLevel");
	FromJson(entity.nShowReqLevel, value, "nShowReqLevel");
	FromJson(entity.nPlayReqVipLevel, value, "nPlayReqVipLevel");
	FromJson(entity.nPlayReqLevel, value, "nPlayReqLevel");
	FromJson(entity.nActvtReferTime, value, "nActvtReferTime");
	FromJson(entity.nActvtStopTime, value, "nActvtStopTime");
	FromJson(entity.nShowStartTime, value, "nShowStartTime");
	FromJson(entity.nShowEndTime, value, "nShowEndTime");
	FromJson(entity.nPlayStartTime, value, "nPlayStartTime");
	FromJson(entity.nPlayEndTime, value, "nPlayEndTime");
	FromJson(entity.nDailyStartTime, value, "nDailyStartTime");
	FromJson(entity.nDailyEndTime, value, "nDailyEndTime");
	FromJson(entity.strOpenServers, value, "strOpenServers");
	FromJson(entity.strNotOpenServers, value, "strNotOpenServers");
	FromJson(entity.strActivityParams, value, "strActivityParams");
	FromJson(entity.strClientParams, value, "strClientParams");
}

template<> void JsonHelper::BlockToJson(const OperatingActivity &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.nId, value, "nId");
	ToJson(entity.strName, value, "strName");
	ToJson(entity.strDesc, value, "strDesc");
	ToJson(entity.nUIType, value, "nUIType");
	ToJson(entity.nType, value, "nType");
	ToJson(entity.nTimes, value, "nTimes");
	ToJson(entity.nPreType, value, "nPreType");
	ToJson(entity.nPreTimes, value, "nPreTimes");
	ToJson(entity.nShowReqVipLevel, value, "nShowReqVipLevel");
	ToJson(entity.nShowReqLevel, value, "nShowReqLevel");
	ToJson(entity.nPlayReqVipLevel, value, "nPlayReqVipLevel");
	ToJson(entity.nPlayReqLevel, value, "nPlayReqLevel");
	ToJson(entity.nActvtReferTime, value, "nActvtReferTime");
	ToJson(entity.nActvtStopTime, value, "nActvtStopTime");
	ToJson(entity.nShowStartTime, value, "nShowStartTime");
	ToJson(entity.nShowEndTime, value, "nShowEndTime");
	ToJson(entity.nPlayStartTime, value, "nPlayStartTime");
	ToJson(entity.nPlayEndTime, value, "nPlayEndTime");
	ToJson(entity.nDailyStartTime, value, "nDailyStartTime");
	ToJson(entity.nDailyEndTime, value, "nDailyEndTime");
	ToJson(entity.strOpenServers, value, "strOpenServers");
	ToJson(entity.strNotOpenServers, value, "strNotOpenServers");
	ToJson(entity.strActivityParams, value, "strActivityParams");
	ToJson(entity.strClientParams, value, "strClientParams");
}

template<> const char *GetTableName<OperatingActivityUIType>()
{
	return "operating_activity_uitypes";
}

template<> const char *GetTableKeyName<OperatingActivityUIType>()
{
	return "";
}

template<> uint64 GetTableKeyValue(const OperatingActivityUIType &entity)
{
	return 0;
}

template<> void SetTableKeyValue(OperatingActivityUIType &entity, uint64 key)
{
}

template<> const char *GetTableFieldNameByIndex<OperatingActivityUIType>(size_t index)
{
	switch (index)
	{
		case 0: return "nId";
		case 1: return "nUIType";
		case 2: return "strOpenServers";
		case 3: return "strNotOpenServers";
		case 4: return "strClientParams";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<OperatingActivityUIType>(const char *name)
{
	if (strcmp(name, "nId") == 0) return 0;
	if (strcmp(name, "nUIType") == 0) return 1;
	if (strcmp(name, "strOpenServers") == 0) return 2;
	if (strcmp(name, "strNotOpenServers") == 0) return 3;
	if (strcmp(name, "strClientParams") == 0) return 4;
	return -1;
}

template<> size_t GetTableFieldNumber<OperatingActivityUIType>()
{
	return 5;
}

template<> std::string GetTableFieldValue(const OperatingActivityUIType &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.nId);
		case 1: return StringHelper::ToString(entity.nUIType);
		case 2: return StringHelper::ToString(entity.strOpenServers);
		case 3: return StringHelper::ToString(entity.strNotOpenServers);
		case 4: return StringHelper::ToString(entity.strClientParams);
	}
	return "";
}

template<> void SetTableFieldValue(OperatingActivityUIType &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.nId, value);
		case 1: return StringHelper::FromString(entity.nUIType, value);
		case 2: return StringHelper::FromString(entity.strOpenServers, value);
		case 3: return StringHelper::FromString(entity.strNotOpenServers, value);
		case 4: return StringHelper::FromString(entity.strClientParams, value);
	}
}

template<> void LoadFromStream(OperatingActivityUIType &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.nId, stream);
	StreamHelper::FromStream(entity.nUIType, stream);
	StreamHelper::FromStream(entity.strOpenServers, stream);
	StreamHelper::FromStream(entity.strNotOpenServers, stream);
	StreamHelper::FromStream(entity.strClientParams, stream);
}

template<> void SaveToStream(const OperatingActivityUIType &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.nId, stream);
	StreamHelper::ToStream(entity.nUIType, stream);
	StreamHelper::ToStream(entity.strOpenServers, stream);
	StreamHelper::ToStream(entity.strNotOpenServers, stream);
	StreamHelper::ToStream(entity.strClientParams, stream);
}

template<> void LoadFromText(OperatingActivityUIType &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const OperatingActivityUIType &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(OperatingActivityUIType &entity, const rapidjson::Value &value)
{
	FromJson(entity.nId, value, "nId");
	FromJson(entity.nUIType, value, "nUIType");
	FromJson(entity.strOpenServers, value, "strOpenServers");
	FromJson(entity.strNotOpenServers, value, "strNotOpenServers");
	FromJson(entity.strClientParams, value, "strClientParams");
}

template<> void JsonHelper::BlockToJson(const OperatingActivityUIType &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.nId, value, "nId");
	ToJson(entity.nUIType, value, "nUIType");
	ToJson(entity.strOpenServers, value, "strOpenServers");
	ToJson(entity.strNotOpenServers, value, "strNotOpenServers");
	ToJson(entity.strClientParams, value, "strClientParams");
}

template<> void LoadFromStream(OperatingPlayType &entity, std::istream &stream)
{
	StreamHelper::EnumFromStream(entity, stream);
}

template<> void SaveToStream(const OperatingPlayType &entity, std::ostream &stream)
{
	StreamHelper::EnumToStream(entity, stream);
}

template<> void LoadFromText(OperatingPlayType &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const OperatingPlayType &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(OperatingPlayType &entity, const rapidjson::Value &value)
{
	EnumFromJson(entity, value);
}

template<> void JsonHelper::BlockToJson(const OperatingPlayType &entity, rapidjson::Value &value)
{
	EnumToJson(entity, value);
}

template<> void LoadFromStream(OperatingChargeSelfSelect::RewardData &entity, std::istream &stream)
{
	StreamHelper::BlockSequenceFromStream(entity.rewardItems, stream);
}

template<> void SaveToStream(const OperatingChargeSelfSelect::RewardData &entity, std::ostream &stream)
{
	StreamHelper::BlockSequenceToStream(entity.rewardItems, stream);
}

template<> void LoadFromText(OperatingChargeSelfSelect::RewardData &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const OperatingChargeSelfSelect::RewardData &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(OperatingChargeSelfSelect::RewardData &entity, const rapidjson::Value &value)
{
	BlockSequenceFromJson(entity.rewardItems, value, "rewardItems");
}

template<> void JsonHelper::BlockToJson(const OperatingChargeSelfSelect::RewardData &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	BlockSequenceToJson(entity.rewardItems, value, "rewardItems");
}

template<> void LoadFromStream(OperatingChargeSelfSelect &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.gradVal, stream);
	StreamHelper::FromStream(entity.maxTimes, stream);
	StreamHelper::BlockSequenceFromStream(entity.rewardDataList, stream);
}

template<> void SaveToStream(const OperatingChargeSelfSelect &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.gradVal, stream);
	StreamHelper::ToStream(entity.maxTimes, stream);
	StreamHelper::BlockSequenceToStream(entity.rewardDataList, stream);
}

template<> void LoadFromText(OperatingChargeSelfSelect &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const OperatingChargeSelfSelect &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(OperatingChargeSelfSelect &entity, const rapidjson::Value &value)
{
	FromJson(entity.gradVal, value, "gradVal");
	FromJson(entity.maxTimes, value, "maxTimes");
	BlockSequenceFromJson(entity.rewardDataList, value, "rewardDataList");
}

template<> void JsonHelper::BlockToJson(const OperatingChargeSelfSelect &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.gradVal, value, "gradVal");
	ToJson(entity.maxTimes, value, "maxTimes");
	BlockSequenceToJson(entity.rewardDataList, value, "rewardDataList");
}

template<> void LoadFromStream(OperatingAccumulativeCharge::RewardData &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.gradVal, stream);
	StreamHelper::BlockSequenceFromStream(entity.rewardItems, stream);
}

template<> void SaveToStream(const OperatingAccumulativeCharge::RewardData &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.gradVal, stream);
	StreamHelper::BlockSequenceToStream(entity.rewardItems, stream);
}

template<> void LoadFromText(OperatingAccumulativeCharge::RewardData &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const OperatingAccumulativeCharge::RewardData &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(OperatingAccumulativeCharge::RewardData &entity, const rapidjson::Value &value)
{
	FromJson(entity.gradVal, value, "gradVal");
	BlockSequenceFromJson(entity.rewardItems, value, "rewardItems");
}

template<> void JsonHelper::BlockToJson(const OperatingAccumulativeCharge::RewardData &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.gradVal, value, "gradVal");
	BlockSequenceToJson(entity.rewardItems, value, "rewardItems");
}

template<> void LoadFromStream(OperatingAccumulativeCharge &entity, std::istream &stream)
{
	StreamHelper::BlockSequenceFromStream(entity.rewardDataList, stream);
}

template<> void SaveToStream(const OperatingAccumulativeCharge &entity, std::ostream &stream)
{
	StreamHelper::BlockSequenceToStream(entity.rewardDataList, stream);
}

template<> void LoadFromText(OperatingAccumulativeCharge &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const OperatingAccumulativeCharge &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(OperatingAccumulativeCharge &entity, const rapidjson::Value &value)
{
	BlockSequenceFromJson(entity.rewardDataList, value, "rewardDataList");
}

template<> void JsonHelper::BlockToJson(const OperatingAccumulativeCharge &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	BlockSequenceToJson(entity.rewardDataList, value, "rewardDataList");
}
