#include "preHeader.h"
#include "DBPServerMaster.h"

int main(int argc, char *argv[])
{
	DBPServerMaster::newInstance();
	sDBPServerMaster.InitSingleton();

	if (sDBPServerMaster.Initialize(argc, argv) == 0) {
		sDBPServerMaster.Run(argc, argv);
	}

	sDBPServerMaster.FinishSingleton();
	DBPServerMaster::deleteInstance();

	printf("DBP Server shutdown gracefully!\n");

	return 0;
}
