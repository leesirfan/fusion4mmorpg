#pragma once

#include "Singleton.h"
#include "ServerMaster.h"

class DBPServerMaster : public IServerMaster, public Singleton<DBPServerMaster>
{
public:
	DBPServerMaster();
	virtual ~DBPServerMaster();

	virtual bool InitSingleton();
	virtual void FinishSingleton();

private:
	virtual bool InitDBPool();
	virtual bool LoadDBData();

	virtual bool StartServices();
	virtual void StopServices();

	virtual void Tick();

	virtual std::string GetConfigFile();
	virtual size_t GetDeferAsyncServiceCount();
	virtual size_t GetAsyncServiceCount();
	virtual size_t GetIOServiceCount();
};

#define sDBPServerMaster (*DBPServerMaster::instance())
