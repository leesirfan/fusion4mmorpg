#include "preHeader.h"
#include "GameServerMaster.h"

int main(int argc, char *argv[])
{
	GameServerMaster::newInstance();
	sGameServerMaster.InitSingleton();

	if (sGameServerMaster.Initialize(argc, argv) == 0) {
		sGameServerMaster.Run(argc, argv);
	}

	sGameServerMaster.FinishSingleton();
	GameServerMaster::deleteInstance();

	printf("Game Server shutdown gracefully!\n");

	return 0;
}
