#pragma once

#include "Singleton.h"
#include "MessageHandler.h"
#include "protocol/OpCode.h"

class Account;

class ClientPacketHandler :
	public MessageHandler<ClientPacketHandler, Account, GAME_OPCODE::CSMSG_COUNT>,
	public Singleton<ClientPacketHandler>
{
public:
	ClientPacketHandler();
	virtual ~ClientPacketHandler();
private:
	int HandleCharacterCreate(Account *pAccount, INetPacket &pck);
	int HandleCharacterDelete(Account *pAccount, INetPacket &pck);
	int HandleCharacterList(Account *pAccount, INetPacket &pck);
	int HandleCharacterLogin(Account *pAccount, INetPacket &pck);
	int HandleCharacterLogout(Account *pAccount, INetPacket &pck);
	int HandlePlayerLoadMapFinish(Account *pAccount, INetPacket &pck);
	int HandleChatMessage(Account *pAccount, INetPacket &pck);
	int HandleTeamCreate(Account *pAccount, INetPacket &pck);
	int HandleTeamInvite(Account *pAccount, INetPacket &pck);
	int HandleTeamInviteResp(Account *pAccount, INetPacket &pck);
	int HandleTeamApply(Account *pAccount, INetPacket &pck);
	int HandleTeamApplyResp(Account *pAccount, INetPacket &pck);
	int HandleTeamLeave(Account *pAccount, INetPacket &pck);
	int HandleTeamKick(Account *pAccount, INetPacket &pck);
	int HandleTeamTransfer(Account *pAccount, INetPacket &pck);
	int HandleGuildCreate(Account *pAccount, INetPacket &pck);
	int HandleGuildInvite(Account *pAccount, INetPacket &pck);
	int HandleGuildInviteResp(Account *pAccount, INetPacket &pck);
	int HandleGuildApply(Account *pAccount, INetPacket &pck);
	int HandleGuildApplyResp(Account *pAccount, INetPacket &pck);
	int HandleGuildLeave(Account *pAccount, INetPacket &pck);
	int HandleGuildKick(Account *pAccount, INetPacket &pck);
	int HandleGuildRise(Account *pAccount, INetPacket &pck);
	int HandleGuildDisband(Account *pAccount, INetPacket &pck);
	int HandleGuildGetApply(Account *pAccount, INetPacket &pck);
	int HandleGetShopItemList(Account *pAccount, INetPacket &pck);
	int HandleBuySpecialShopItem(Account *pAccount, INetPacket &pck);
	int HandlePlaybossJoin(Account *pAccount, INetPacket &pck);
};

#define sClientPacketHandler (*ClientPacketHandler::instance())
