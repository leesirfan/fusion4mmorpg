#pragma once

#include "network/Session.h"

class GateServerSessionHandler;

class GateServerSession : public Session
{
public:
	GateServerSession(uint32 sn);
	virtual ~GateServerSession();

	virtual int HandlePacket(INetPacket *pck);
	virtual void OnManaged();
	virtual void OnShutdownSession();

	uint32 sn() const { return m_sn; }
	bool IsReady() const { return m_isReady; }

private:
	int HandleClientPacket(uint32 uid, INetPacket& pck) const;

	friend GateServerSessionHandler;
	const uint32 m_sn;
	bool m_isReady;
};
