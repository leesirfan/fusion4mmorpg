#pragma once

#include "Singleton.h"
#include "ServerMaster.h"

class GameServerMaster : public IServerMaster, public Singleton<GameServerMaster>
{
public:
	GameServerMaster();
	virtual ~GameServerMaster();

	virtual bool InitSingleton();
	virtual void FinishSingleton();

private:
	virtual bool InitDBPool();
	virtual bool LoadDBData();

	virtual bool StartServices();
	virtual void StopServices();

	virtual void Tick();

	virtual std::string GetConfigFile();
	virtual size_t GetDeferAsyncServiceCount();
	virtual size_t GetAsyncServiceCount();
	virtual size_t GetIOServiceCount();
};

#define sGameServerMaster (*GameServerMaster::instance())
