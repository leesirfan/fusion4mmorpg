#include "preHeader.h"
#include "Character.h"
#include "Account.h"

Character::Character(ObjGUID guid, uint32 acct, uint32 gsId)
: m_pAccount(NULL)
, guid(guid)
, acct(acct)
, gsId(gsId)
, career(0)
, gender(0)
, lastVipLevel(0)
, lastLevel(0)
, lastPercHP(0)
, lastPercMP(0)
, lastInstOwner(ObjGUID_NULL)
, lastInstGuid(InstGUID_NULL)
, guildId(0)
, guildTitle(0)
, isOnline(false)
, lastOnlineTime(0)
, serverFlags(1<<eUninitialized)
, socialData(this)
, pTeam(NULL)
{
}

Character::~Character()
{
}

void Character::SendError(GErrorCode error) const
{
	if (m_pAccount != NULL) {
		m_pAccount->SendError(error);
	}
}

void Character::SendError(const GErrorCodeP1& error) const
{
	if (m_pAccount != NULL) {
		m_pAccount->SendError(error);
	}
}

void Character::SendError(const GErrorCodeP2& error) const
{
	if (m_pAccount != NULL) {
		m_pAccount->SendError(error);
	}
}

void Character::SendError(const GErrorCodeP3& error) const
{
	if (m_pAccount != NULL) {
		m_pAccount->SendError(error);
	}
}

void Character::SendError(const GErrorInfo& error) const
{
	if (m_pAccount != NULL) {
		m_pAccount->SendError(error);
	}
}

void Character::SendPacket(const INetPacket& pck) const
{
	if (m_pAccount != NULL) {
		m_pAccount->SendPacket(pck);
	}
}
