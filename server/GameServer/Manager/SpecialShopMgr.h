#pragma once

#include "Singleton.h"

class Character;

class SpecialShopMgr : public Singleton<SpecialShopMgr>
{
public:
	SpecialShopMgr();
	virtual ~SpecialShopMgr();

	void LoadData();

	GErrorCode HandleBuySpecialShopItem(Coroutine::YieldContext& ctx,
		Character* pChar, uint64 itemUniqueKey, uint32 num);

	void PackShopItemList(INetPacket& pck, uint32 shopType) const;

private:
	void SaveSpecialShopStatus();
	void LoadSpecialShopStatus();

	std::unordered_map<uint64, ShopItemStatus> m_specialShopStatus;
	std::unordered_map<uint64, SpecialShopPrototype> m_specialShopPrototypes;
};

#define sSpecialShopMgr (*SpecialShopMgr::instance())
