#pragma once

#include "Singleton.h"

#define GuildLeagueJoinRankBest (16)

class Character;

class GuildLeagueMgr : public WheelTriggerOwner, public WheelTwinklerOwner,
	public Singleton<GuildLeagueMgr>
{
public:
	GuildLeagueMgr();
	virtual ~GuildLeagueMgr();

	void Init();
	void LoadData();

	void CleanInvalidGuilds(const std::unordered_set<uint32>& guildIds);

	GErrorCode HandleJoinGuildLeague(Character* pChar);

	GErrorCode OnFinishGuildLeague(
		InstGUID instGuid, uint32 winGuildId, time_t startTime);

	std::pair<uint32, uint32> GetGuildLeagueMember(
		InstGUID instGuid, time_t startTime);

private:
	void LockGuildLeagueParticipant();
	void StartFirstRoundGuildLeague();
	void StopFirstRoundGuildLeague();
	void StartSecondRoundGuildLeague();
	void StopSecondRoundGuildLeague();

	void SaveGuildLeagueStatus();
	void LoadGuildLeagueStatus();

	void EnsureGuildLeagueInstance(size_t instPos);
	void FastFinishGuildLeagueInstance(size_t instPos);
	void ResetAllGuildLeagueInstances();

	bool IsGuildLeagueInstantiate(size_t instPos) const;
	size_t GetGuildLeagueInstancePos(size_t fightPos) const;
	size_t GetGuildLeagueFightPos(uint32 guildId) const;

	std::pair<uint32&, uint32&> GetGuildLeagueMember(size_t instPos);

	virtual WheelTimerMgr *GetWheelTimerMgr();

	bool m_isFirstLeague;
	std::vector<uint32> m_guildLeagueRanks;
	size_t m_keepUpLeagueTimes;

	std::vector<uint32> m_guildLeagueFights;
	time_t m_lastLockLeagueTime;
	time_t m_leagueStartTime;
	time_t m_leaguePrepareTime;
	time_t m_leagueDurationTime;
	const uint32(*m_pBattleTable)[2][2];

	InstGUID m_guildLeagueInsts[(GuildLeagueJoinRankBest / 4 + 1) * 2];
	uint32 m_instSeed;
};

#define sGuildLeagueMgr (*GuildLeagueMgr::instance())
