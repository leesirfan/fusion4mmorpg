#pragma once

enum class ATTRPARTTYPE
{
	BASE,
	BODY,
	EQUIP,
	BUFF,
	COUNT,
	ALL = ~0,
};

enum class ATTREXTYPE
{
	NONE,
	DAMAGE_FACTOR,
	RECOVERY_HP_RATE,
	RECOVERY_HP_VALUE,
	RECOVERY_MP_RATE,
	RECOVERY_MP_VALUE,
	CANT_LOSE_HP,
	CANT_DEAD,
	COUNT
};

enum class ATTRVEXTYPE
{
	NONE,
	COUNT
};

enum class ATTRSEXTYPE
{
	NONE,
	COUNT
};
