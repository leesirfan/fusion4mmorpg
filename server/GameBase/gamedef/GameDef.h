#pragma once

#define BOSS_RANK_MAX (10)

enum PlayBossSyncType {
	PBST_SyncMember,
	PBST_SyncBoss,
};

bool IsInDameDays(const std::vector<int>& days,
	TriggerCycle triggerCycle);
bool IsInGameTime(const TriggerPoint& triggerPoint,
	TriggerCycle triggerCycle, time_t actvtTime);
TextUnpacker GetGameTimeUnpacker(GameTime::Type type);
std::vector<int> UnpackGameTime4Days(TextUnpacker& unpacker);
TriggerPoint UnpackGameTime4Trigger(
	TextUnpacker& unpacker, TriggerCycle triggerCycle);
std::pair<TriggerPoint, time_t> UnpackGameTime4Twinkler(
	TextUnpacker& unpacker, TriggerCycle triggerCycle);
