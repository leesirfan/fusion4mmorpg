#pragma once

enum class PullValueFlag
{
	ExtraKey = 1 << 0,
	NonDict = 1 << 1,
};

enum class CharValueType
{
	Key = 1 << 0,
	Name = 1 << 1,
	Level = 1 << 2,
	FightValue = 1 << 3,
	LastOnlineTime = 1 << 4,
	All = -1,
};

enum class GuildValueType
{
	Key = 1 << 0,
	Name = 1 << 1,
	Level = 1 << 2,
	All = -1,
};
