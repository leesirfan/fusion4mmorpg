#include "preHeader.h"
#include "GameDef.h"

bool IsInDameDays(const std::vector<int>& days,
	TriggerCycle triggerCycle)
{
	assert(!days.empty());
	if (days[0] == -1) {
		return true;
	}
	switch (triggerCycle) {
	case ByMonthlyTrigger:
		return IS_VECTOR_CONTAIN_VALUE(days, GET_DATE_TIME.tm_mday);
	default:
		return IS_VECTOR_CONTAIN_VALUE(days, GET_DATE_TIME.tm_wday);
	}
}

bool IsInGameTime(const TriggerPoint& triggerPoint,
	TriggerCycle triggerCycle, time_t actvtTime)
{
	time_t lastTriggerTime;
	switch (triggerCycle) {
	case ByMonthlyTrigger:
		lastTriggerTime = CalcPreviousTriggerPointTimeByMonthly(triggerPoint);
		break;
	default:
		lastTriggerTime = CalcPreviousTriggerPointTime(triggerCycle, triggerPoint);
		break;
	}
	return GET_UNIX_TIME < lastTriggerTime + actvtTime;
}

TextUnpacker GetGameTimeUnpacker(GameTime::Type type)
{
	auto pGameTime = GetDBEntry<GameTime>((u64)type);
	return {pGameTime != NULL ? pGameTime->strTime.c_str() : ""};
}

std::vector<int> UnpackGameTime4Days(TextUnpacker& unpacker)
{
	std::vector<int> days;
	while (!unpacker.IsDelimiter(';')) {
		days.push_back(unpacker.Unpack<int>());
	}
	return days;
}

TriggerPoint UnpackGameTime4Trigger(
	TextUnpacker& unpacker, TriggerCycle triggerCycle)
{
	int8 wday = -1;
	uint8 hour = 0, min = 0, sec = 0;
	switch (triggerCycle) {
	case ByMonthlyTrigger:
	case ByWeeklyTrigger:
		unpacker >> wday;
	case ByDailyTrigger:
		unpacker >> hour;
	case ByHourlyTrigger:
		unpacker >> min;
	case ByMinutelyTrigger:
		unpacker >> sec;
	}
	return MakeTriggerPoint(wday, hour, min, sec);
}

std::pair<TriggerPoint, time_t> UnpackGameTime4Twinkler(
	TextUnpacker& unpacker, TriggerCycle triggerCycle)
{
	std::pair<TriggerPoint, time_t> rst;
	rst.first = UnpackGameTime4Trigger(unpacker, triggerCycle);
	rst.second = unpacker.Unpack<time_t>();
	return rst;
}
