#pragma once

#define PLAYER_UID_MIN (100)

enum CHEQUE_FLOW_TYPE
{
	CFT_NONE,
	CFT_SCRIPT,  // 脚本
	CFT_PAY,  // 充值
	CFT_SHOP,  // 商店
	CFT_MAIL,  // 邮件
	CFT_LOOT,  // 掉落
	CFT_MINE,  // 采集
	CFT_ITEM_USE,  // 使用道具
	CFT_QUEST_ACCEPT,  // 接受任务
	CFT_QUEST_CANCEL,  // 取消任务
	CFT_QUEST_SUBMIT,  // 提交任务
	CFT_CREATE_GUILD,  // 创建帮派
	CFT_OPERATING_BEGIN,  // 运营活动
	CFT_OPERATING_END = CFT_OPERATING_BEGIN + 100,
	CFT_MAX
};

enum IPCF32_PROPERTIES
{
	IPCF32_NONE,
	IPCF32_MAX
};

enum IPCS32_PROPERTIES
{
	IPCS32_NONE,
	IPCS32_MAX
};

enum IPCS64_PROPERTIES
{
	IPCS64_NONE,
	IPCS64_MAX
};

enum PlayerMapState
{
	PlayerEnterMapState,
	PlayerStrayMapState,
	PlayerInMapState,
	PlayerLeaveMapState,
};

enum PlayerStorageFlag
{
	PSF_INCLUDE_PLAYER = 1 << 0,
	PSF_INCLUDE_STRAY_PLAYER = 1 << 1,
	PSF_INCLUDE_AUTO_PLAYER = 1 << 2,
	PSF_EXCLUDE_PLAYER = 1 << 8,
	PSF_EXCLUDE_STRAY_PLAYER = 1 << 9,
	PSF_EXCLUDE_AUTO_PLAYER = 1 << 10,
};

struct CharUpdateInfo
{
	enum class Type {
		VipLevel,
		Level,
		PercHP,
		PercMP,
		Pos,
		FightValue,
		Count
	};
	uint32 lastVipLevel = 0;
	uint32 lastLevel = 0;
	double lastPercHP = 0;
	double lastPercMP = 0;
	vector3f1f lastPos;
	uint64 lastFightValue = 0;
};
