#pragma once

#include "Base.h"
#include "Macro.h"
#include "Debugger.h"
#include "InlineFuncs.h"
#include "Concurrency.h"
#include "Exception.h"
#include "KeyFile.h"
#include "Logger.h"
#include "NetStream.h"
#include "NetBuffer.h"
#include "NetPacket.h"
#include "OS.h"
#include "System.h"
#include "Thread.h"
#include "ThreadPool.h"
#include "ThreadSafeBlockQueue.h"
#include "ThreadSafePool.h"
#include "ThreadSafeQueue.h"
#include "ThreadSafeSet.h"
#include "TextPacker.h"
#include "MultiBufferQueue.h"
#include "noncopyable.h"
#include "enable_linked_from_this.h"

#include "jsontable/table_interface.h"
#include "jsontable/table_utils.h"
#include "network/SessionManager.h"
#include "async/AsyncTaskMgr.h"
#include "timer/WheelTimerMgr.h"
#include "timer/WheelTimerOwner.h"
#include "timer/WheelTriggerOwner.h"
#include "timer/WheelTwinklerOwner.h"
#include "coroutine/Coroutine.h"
#include "lua/LuaFunc.h"
#include "lua/LuaMgr.h"

#include <atomic>
#include <algorithm>
#include <numeric>
#include <functional>
#include <memory>
#include <list>
#include <vector>
#include <bitset>
#include <deque>
#include <queue>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <string>
#include <string_view>
#include <fstream>
#include <future>

#include <stdint.h>
#include <limits.h>
#include <float.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "math/base.h"
#include "math/vector.h"
#include "math/aabb.h"
#include "math/utils.h"

#include "error/GErrorCode.h"
#include "error/DBPError.h"

#include "protocol/OpCode.h"
#include "protocol/InternalProtocol.h"
#include "protocol/DBPProtocol.h"
#include "protocol/CenterProtocol.h"

#include "DB/MysqlDatabasePool.h"
#include "DB/DatabaseMgr.h"

#include "gamedef/TypeDef.h"
#include "gamedef/ObjectDef.h"
#include "gamedef/OperatingDef.h"
#include "gamedef/SObjDef.h"
#include "gamedef/UnitDef.h"
#include "gamedef/CreatureDef.h"
#include "gamedef/PlayerDef.h"
#include "gamedef/AttrDef.h"
#include "gamedef/MapDef.h"
#include "gamedef/ItemDef.h"
#include "gamedef/QuestDef.h"
#include "gamedef/LootDef.h"
#include "gamedef/SpellDef.h"
#include "gamedef/MailDef.h"
#include "gamedef/ChatDef.h"
#include "gamedef/TeamDef.h"
#include "gamedef/TimerDef.h"
#include "gamedef/ShopDef.h"
#include "gamedef/GuildDef.h"
#include "gamedef/RankDef.h"
#include "gamedef/SocialDef.h"
#include "gamedef/ServerDef.h"
#include "gamedef/GameDef.h"

#include "BitMask.h"
#include "GameBase.h"
#include "VarDefines.h"
#include "ScriptHelper.h"
#include "ScriptCallable.h"
#include "FuncUtils.h"

#include "Config/GameConfig.h"

#define DEF_S2S_RPC_TIMEOUT (5)

#define CROSS_SERVER_MAX (1024)
#define SAVE_DELAY_MAX (60)

#define ONE_MINUTE (60)
#define ONE_HOUR (60*60)
#define ONE_DAY (60*60*24)
#define ONE_WEEK (60*60*24*7)
#define ONE_MONTH (60*60*24*30)

#define PutTwoArgs(Type, Name) \
	const Type Name##1, const Type Name##2
#define PutTwoRArgs(Type, Name) \
	const Type& Name##1, const Type& Name##2
#define PutTwoPArgs(Type, Name) \
	const Type* Name##1, const Type* Name##2

template <typename T>
using params = const std::initializer_list<T> &;
