#pragma once

#include "Singleton.h"
#include "MessageHandler.h"
#include "protocol/DBPProtocol.h"

class DBPSession;

class DBPSessionHandler :
	public MessageHandler<DBPSessionHandler, DBPSession, DBPProtocol::DBP_PROTOCOL_COUNT>,
	public Singleton<DBPSessionHandler>
{
public:
	DBPSessionHandler();
	virtual ~DBPSessionHandler();
private:
	int HandleRegisterResp(DBPSession *pSession, INetPacket &pck);
};

#define sDBPSessionHandler (*DBPSessionHandler::instance())
