#pragma once

#include "rpc/RPCActor.h"

class DBPHelper
{
public:
	static RPCError RPCBlockInvoke(RPCActor* actor, const INetPacket& pck,
		const std::function<void(INetStream&)>& cb,
		const std::function<RPCError()>& updater = nullptr);
};
