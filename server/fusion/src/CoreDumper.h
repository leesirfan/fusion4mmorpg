#pragma once

#include "Singleton.h"

class CoreDumper : public Singleton<CoreDumper>
{
public:
    CoreDumper();
    virtual ~CoreDumper();

    void ManualDump();

    void EnableManual() { enable_manual_ = true; }
    void DisableManual() { enable_manual_ = false; }

    void SetDumpUpper(unsigned upper) { dump_upper_ = upper; }

private:
    bool enable_manual_;

    unsigned dump_upper_;
    unsigned dump_count_;
};

#define sCoreDumper (*CoreDumper::instance())
