set(CMAKE_CXX_FLAGS "-Wall -pthread")
set(CMAKE_CXX_STANDARD 11)

include(FindPkgConfig)
pkg_check_modules(3RD REQUIRED libcrypto libcurl liblz4 mysqlclient zlib)
find_package(Boost REQUIRED)

include_directories(. ../external ../feature ../thirdparty ../thirdparty/lua/include
        ${3RD_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS})

aux_source_directory(. SRC_LIST)
aux_source_directory(async ASYNC_SRC_LIST)
aux_source_directory(http HTTP_SRC_LIST)
aux_source_directory(lua LUA_SRC_LIST)
aux_source_directory(lz4 LZ4_SRC_LIST)
aux_source_directory(mysql MYSQL_SRC_LIST)
aux_source_directory(network NETWORK_SRC_LIST)
aux_source_directory(openssl OPENSSL_SRC_LIST)
aux_source_directory(rpc RPC_SRC_LIST)
aux_source_directory(timer TIMER_SRC_LIST)
aux_source_directory(zlib ZLIB_SRC_LIST)
add_library(core STATIC ${SRC_LIST} ${ASYNC_SRC_LIST} ${HTTP_SRC_LIST} ${LUA_SRC_LIST}
        ${LZ4_SRC_LIST} ${MYSQL_SRC_LIST} ${NETWORK_SRC_LIST} ${OPENSSL_SRC_LIST}
        ${RPC_SRC_LIST} ${TIMER_SRC_LIST} ${ZLIB_SRC_LIST})
