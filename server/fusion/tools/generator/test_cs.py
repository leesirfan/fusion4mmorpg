import sys
from table_cs_generator import *

if __name__ == '__main__':
    cfg = CsTableConfig('uint', '0')
    for i in range(1, len(sys.argv), 2):
        to_cs_files(sys.argv[i], sys.argv[i+1], cfg)
