import os

suffixes = {
    '.inl': 'gbk',
    '.h':   'gbk',
    '.cpp': 'gbk',
    '.lua': 'utf-8',
    '.py':  'utf-8',
}

def strip_file(filepath):
    suffix = os.path.splitext(filepath)[1]
    if suffix in suffixes:
        encode = suffixes[suffix]
        with open(filepath, 'r', encoding=encode) as fp:
            lines = fp.readlines()
        with open(filepath, 'wb') as fp:
            for line in lines:
                line = line.rstrip().replace('\t', '\x20' * 4) + '\n'
                fp.write(line.encode(encode))

def strip_directory(dirpath, recursive = True):
    if os.path.exists(dirpath):
        for entry in os.listdir(dirpath):
            entrypath = os.path.join(dirpath, entry)
            if os.path.isfile(entrypath):
                strip_file(entrypath)
            elif os.path.isdir(entrypath):
                if recursive:
                    strip_directory(entrypath)
