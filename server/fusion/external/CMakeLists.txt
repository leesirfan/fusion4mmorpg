set(CMAKE_CXX_FLAGS "-Wall -pthread")
set(CMAKE_CXX_STANDARD 11)

include_directories(../feature)

aux_source_directory(. SRC_LIST)
add_library(external STATIC ${SRC_LIST})
