#include "TextUtils.h"
#include <cstdlib>
#include <cstring>

void WriteTEXT(std::ostream &stream, const char *s, ssize_t n)
{
    size_t len = 0;
    if (n < 0) {
        len = strlen(s);
    } else if (n > 0) {
        const char *endptr = (const char *)memchr(s, '\0', n);
        if (endptr == nullptr) {
            len = n;
        } else {
            len = endptr - s;
        }
    }
    stream << len;
    if (len != 0) {
        (stream << ':').write(s, len);
    }
    stream << ',';
}

std::string_view ReadTEXT(const char *&ptr)
{
    std::string_view s;
    size_t len = ReadUINT32(ptr);
    if (len != 0) {
        const char *endptr = (const char *)memchr(ptr, '\0', len + 1);
        if (endptr != nullptr) {
            s = std::string_view(ptr, endptr - ptr);
            ptr = endptr;
        } else {
            s = std::string_view(ptr, len);
            ptr += len + 1;
        }
    }
    return s;
}

char ReadCHAR(const char *&ptr)
{
    const char ch = *ptr;
    ptr += ptr[0] != '\0' ? (ptr[1] != '\0' ? 2 : 1) : 0;
    return ch;
}

bool ReadBOOL(const char *&ptr)
{
    char *endptr = nullptr;
    long numeric = strtol(ptr, &endptr, 0);
    ptr = *endptr != '\0' ? endptr + 1 : endptr;
    return numeric != 0;
}

float ReadFLOAT(const char *&ptr)
{
    char *endptr = nullptr;
    float numeric = strtof(ptr, &endptr);
    ptr = *endptr != '\0' ? endptr + 1 : endptr;
    return numeric;
}

double ReadDOUBLE(const char *&ptr)
{
    char *endptr = nullptr;
    double numeric = strtod(ptr, &endptr);
    ptr = *endptr != '\0' ? endptr + 1 : endptr;
    return numeric;
}

int32 ReadINT32(const char *&ptr, int base)
{
    char *endptr = nullptr;
    int32 numeric = strtol(ptr, &endptr, base);
    ptr = *endptr != '\0' ? endptr + 1 : endptr;
    return numeric;
}

int64 ReadINT64(const char *&ptr, int base)
{
    char *endptr = nullptr;
    int64 numeric = strtoll(ptr, &endptr, base);
    ptr = *endptr != '\0' ? endptr + 1 : endptr;
    return numeric;
}

uint32 ReadUINT32(const char *&ptr, int base)
{
    char *endptr = nullptr;
    uint32 numeric = strtoul(ptr, &endptr, base);
    ptr = *endptr != '\0' ? endptr + 1 : endptr;
    return numeric;
}

uint64 ReadUINT64(const char *&ptr, int base)
{
    char *endptr = nullptr;
    uint64 numeric = strtoull(ptr, &endptr, base);
    ptr = *endptr != '\0' ? endptr + 1 : endptr;
    return numeric;
}
