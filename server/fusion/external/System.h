#pragma once

#include <ctime>
#include <atomic>
#include <memory>
#include <random>
#include "Base.h"
#include "Macro.h"
#include "CxxFeature.h"

class System
{
public:
    struct AtomicDateTime {
        struct tm date_;
        time_t time_;
    };

    static void Init();
    static void Update();

    static s64 Rand(s64 lower, s64 upper, bool closed = false);
    static s64 RandS64(s64 lower, s64 upper, bool closed = false);

    static int Rand(int lower, int upper, bool closed = false);
    static int Randi(int lower, int upper, bool closed = false);
    static float Rand(float lower, float upper);
    static float Randf(float lower, float upper);

    static time_t GetDayUnixTime(time_t t);
    static time_t GetWeekUnixTime(time_t t);
    static time_t GetMonthUnixTime(time_t t);

    static time_t GetDayUnixTime(const AtomicDateTime &t);
    static time_t GetWeekUnixTime(const AtomicDateTime &t);
    static time_t GetMonthUnixTime(const AtomicDateTime &t);

    static time_t GetDayUnixTime();
    static time_t GetWeekUnixTime();
    static time_t GetMonthUnixTime();
    static struct tm GetDateTime();

    static uint64 GetRealSysTime();

    static time_t GetUnixTime() { return unix_time_; }
    static uint64 GetSysTime() { return sys_time_; }
    static uint64 GetStartTime() { return start_time_; }

    static std::shared_ptr<const AtomicDateTime> GetAtomicDateTime() {
        return date_time_.load();
    }

private:
    static time_t unix_time_;
    static uint64 sys_time_;
    static uint64 start_time_;
    static std::atomic<std::shared_ptr<AtomicDateTime>> date_time_;
#if defined(_WIN32)
    static LARGE_INTEGER performance_frequency_;
#endif
    static std::default_random_engine random_engine_;
};

#define GET_UNIX_TIME (System::GetUnixTime())
#define GET_SYS_TIME (System::GetSysTime())
#define GET_DATE_TIME (System::GetDateTime())
#define GET_ATOMIC_DATE_TIME (System::GetAtomicDateTime())

#define GET_MINUTE_UNIX_TIME (System::GetMinuteUnixTime())
#define GET_DAY_UNIX_TIME (System::GetDayUnixTime())
#define GET_WEEK_UNIX_TIME (System::GetWeekUnixTime())
#define GET_MONTH_UNIX_TIME (System::GetMonthUnixTime())

#define GET_REAL_SYS_TIME (System::GetRealSysTime())

#define GET_APP_TIME (System::GetSysTime() - System::GetStartTime())
#define GET_REAL_APP_TIME (System::GetRealSysTime() - System::GetStartTime())
