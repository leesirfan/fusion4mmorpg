#pragma once

#include <cstdlib>
#include "InlineFuncs.h"
#include "ThreadSafePool.h"

// be useful for:
// multi producer, single or multi consumer.

#define S_MAX_BUFFER_QUEUE_POOL_COUNT (8)

template <typename T, size_t N = 128>
class MultiBufferQueue
{
    struct DataQueue {
        DataQueue *next = nullptr;
        size_t rpos = 0, wpos = 0;
        T queue[N];
    };

public:
    MultiBufferQueue() {
        std::call_once(s_queue_flag_, AutoQueuePool);
        tail_ = head_ = AllocQueue();
    }
    ~MultiBufferQueue() {
        do {
            auto next = head_->next;
            FreeQueue(head_);
            head_ = next;
        } while (head_ != nullptr);
    }

    bool IsEmpty() {
        std::lock_guard<std::mutex> lock(w_lock_);
        return tail_->rpos >= tail_->wpos;
    }

    size_t GetSize() {
        size_t n = 0;
        for (auto queue = head_; queue != nullptr; queue = queue->next) {
            n += queue->wpos - queue->rpos;
        }
        return n;
    }

    void Enqueue(const T &v) {
        Enqueue((T&&)T{v});
    }

    void Enqueue(T &&v) {
        std::lock_guard<std::mutex> lock(w_lock_);
        if (tail_->wpos >= N) {
            tail_ = tail_->next = AllocQueue();
        }
        tail_->queue[tail_->wpos] = std::move(v);
        tail_->wpos += 1;
    }

    bool Dequeue(T &v) {
mark:   if (head_->rpos < head_->wpos) {
            v = std::move(head_->queue[head_->rpos++]);
            return true;
        }
        if (head_->rpos >= N && head_->next != nullptr) {
            auto next = head_->next;
            FreeQueue(head_);
            head_ = next;
            goto mark;
        }
        return false;
    }

    bool DequeueSafe(T &v) {
        DataQueue *head = nullptr, *tail = nullptr;
        bool isOK = false;
        do {
            std::lock_guard<std::mutex> lock(r_lock_);
mark:       if (head_->rpos < head_->wpos) {
                v = std::move(head_->queue[head_->rpos++]);
                isOK = true;
                break;
            }
            if (head_->rpos >= N && head_->next != nullptr) {
                if (tail == nullptr) {
                    tail = head = head_;
                } else {
                    tail = tail->next = head_;
                }
                head_ = head_->next;
                goto mark;
            }
        } while (0);
        while (head != nullptr) {
            auto next = head->next;
            FreeQueue(head);
            head = next;
        }
        return isOK;
    }

private:
    DataQueue *head_, *tail_;
    std::mutex r_lock_, w_lock_;

private:
    static void AutoQueuePool() {
        InitQueuePool();
        std::atexit(ClearQueuePool);
    }

    static void InitQueuePool() {
    }
    static void ClearQueuePool() {
        DataQueue *queue = nullptr;
        while ((queue = s_queue_pool_.Get()) != nullptr) {
            delete queue;
        }
    }

    static DataQueue *AllocQueue() {
        DataQueue *queue = nullptr;
        if ((queue = s_queue_pool_.Get()) != nullptr) {
            return REINIT_OBJECT(queue);
        } else {
            return new DataQueue;
        }
    }
    static void FreeQueue(DataQueue *queue) {
        if (!s_queue_pool_.Put(queue)) {
            delete queue;
        }
    }

    static std::once_flag s_queue_flag_;
    static ThreadSafePool<DataQueue, S_MAX_BUFFER_QUEUE_POOL_COUNT>
        s_queue_pool_;
};

template <typename T, size_t N>
std::once_flag MultiBufferQueue<T, N>::s_queue_flag_;
template <typename T, size_t N>
ThreadSafePool<
    typename MultiBufferQueue<T, N>::DataQueue,
    S_MAX_BUFFER_QUEUE_POOL_COUNT
> MultiBufferQueue<T, N>::s_queue_pool_;
