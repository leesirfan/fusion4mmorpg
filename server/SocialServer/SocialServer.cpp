#include "preHeader.h"
#include "SocialServer.h"
#include "Game/PacketDispatcher.h"
#include "Game/TransMgr.h"
#include "Guild/GuildMgr.h"
#include "ServerMaster.h"

SocialServer::SocialServer()
: sWheelTimerMgr(1, GET_UNIX_TIME)
, m_isServing(false)
{
}

SocialServer::~SocialServer()
{
}

WheelTimerMgr *SocialServer::GetWheelTimerMgr()
{
	return &sWheelTimerMgr;
}

bool SocialServer::Initialize()
{
	if (!sGuildMgr.LoadDataFromDB()) {
		ELOG("Server Load Guild Data Error!!!");
		IServerMaster::GetInstance().End(0);
		return false;
	}
	m_isServing = true;
	NLOG("Social Server Start Serving ...");
	return FrameWorker::Initialize();
}

void SocialServer::Finish()
{
	sSessionManager.Stop();
	return FrameWorker::Finish();
}

void SocialServer::Update(uint64 diffTime)
{
	AsyncTaskOwner::UpdateTask();
	sSessionManager.Update();
}

void SocialServer::OnTick()
{
	sTransMgr.RunGC();
	sSessionManager.Tick();
	sPacketDispatcher.Tick();
	sWheelTimerMgr.Update(GET_UNIX_TIME);
}
