#pragma once

#include "Singleton.h"
#include "FrameWorker.h"

class SocialServer : public FrameWorker, public AsyncTaskOwner,
	public WheelTimerOwner, public Singleton<SocialServer>
{
public:
	THREAD_RUNTIME(SocialServer)

	SocialServer();
	virtual ~SocialServer();

	bool IsServing() const { return m_isServing; }

	WheelTimerMgr sWheelTimerMgr;

private:
	virtual WheelTimerMgr *GetWheelTimerMgr();

	virtual bool Initialize();
	virtual void Finish();

	virtual void Update(uint64 diffTime);
	virtual void OnTick();

	bool m_isServing;
};

#define sSocialServer (*SocialServer::instance())
