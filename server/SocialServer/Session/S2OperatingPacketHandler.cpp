#include "preHeader.h"
#include "S2OperatingPacketHandler.h"
#include "protocol/InternalProtocol.h"

S2OperatingPacketHandler::S2OperatingPacketHandler()
{
	handlers_[GameServer2SocialServer::CGX_OPERATING_GET_LIST] = &S2OperatingPacketHandler::HandleOperatingGetList;
	handlers_[GameServer2SocialServer::CGX_OPERATING_GET_DETAIL] = &S2OperatingPacketHandler::HandleOperatingGetDetail;
	handlers_[GameServer2SocialServer::CGX_OPERATING_GET_SHORT_HOT] = &S2OperatingPacketHandler::HandleOperatingGetShortHot;
	handlers_[GameServer2SocialServer::CGX_OPERATING_BUY] = &S2OperatingPacketHandler::HandleOperatingBuy;
	handlers_[GameServer2SocialServer::CGX_OPERATING_GET_REWARD] = &S2OperatingPacketHandler::HandleOperatingGetReward;
	handlers_[GameServer2SocialServer::CGX_OPERATING_LOGIN] = &S2OperatingPacketHandler::HandleOperatingLogin;
	handlers_[GameServer2SocialServer::CGX_OPERATING_PAY] = &S2OperatingPacketHandler::HandleOperatingPay;
	handlers_[GameServer2SocialServer::CGX_OPERATING_COST] = &S2OperatingPacketHandler::HandleOperatingCost;
	handlers_[GameServer2SocialServer::CGX_OPERATING_GAIN_SCORE] = &S2OperatingPacketHandler::HandleOperatingGainScore;
	handlers_[GameServer2SocialServer::CGX_OPERATING_PLAY_ACTVT] = &S2OperatingPacketHandler::HandleOperatingPlayActvt;
	handlers_[GameServer2SocialServer::CGX_OPERATING_EVENT_ACTVT] = &S2OperatingPacketHandler::HandleOperatingEventActvt;
};

S2OperatingPacketHandler::~S2OperatingPacketHandler()
{
}
