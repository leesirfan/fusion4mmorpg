#include "preHeader.h"
#include "C2GuildPacketHandler.h"
#include "protocol/OpCode.h"

C2GuildPacketHandler::C2GuildPacketHandler()
{
	handlers_[GAME_OPCODE::CMSG_GUILD_GET_LIST] = &C2GuildPacketHandler::HandleGuildGetList;
	handlers_[GAME_OPCODE::CMSG_GUILD_GET_INFO] = &C2GuildPacketHandler::HandleGuildGetInfo;
	handlers_[GAME_OPCODE::CMSG_GUILD_GET_MEMBER] = &C2GuildPacketHandler::HandleGuildGetMember;
};

C2GuildPacketHandler::~C2GuildPacketHandler()
{
}
