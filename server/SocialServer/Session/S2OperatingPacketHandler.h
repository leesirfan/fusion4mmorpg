#pragma once

#include "Singleton.h"
#include "MessageHandler.h"
#include "protocol/InternalProtocol.h"

class OperatingActivityMgr;

class S2OperatingPacketHandler :
	public MessageHandler<S2OperatingPacketHandler, OperatingActivityMgr, GameServer2SocialServer::GAME2SOCIAL_MESSAGE_COUNT>,
	public Singleton<S2OperatingPacketHandler>
{
public:
	S2OperatingPacketHandler();
	virtual ~S2OperatingPacketHandler();
private:
	int HandleOperatingGetList(OperatingActivityMgr *mgr, INetPacket &pck);
	int HandleOperatingGetDetail(OperatingActivityMgr *mgr, INetPacket &pck);
	int HandleOperatingGetShortHot(OperatingActivityMgr *mgr, INetPacket &pck);
	int HandleOperatingBuy(OperatingActivityMgr *mgr, INetPacket &pck);
	int HandleOperatingGetReward(OperatingActivityMgr *mgr, INetPacket &pck);
	int HandleOperatingLogin(OperatingActivityMgr *mgr, INetPacket &pck);
	int HandleOperatingPay(OperatingActivityMgr *mgr, INetPacket &pck);
	int HandleOperatingCost(OperatingActivityMgr *mgr, INetPacket &pck);
	int HandleOperatingGainScore(OperatingActivityMgr *mgr, INetPacket &pck);
	int HandleOperatingPlayActvt(OperatingActivityMgr *mgr, INetPacket &pck);
	int HandleOperatingEventActvt(OperatingActivityMgr *mgr, INetPacket &pck);
};

#define sS2OperatingPacketHandler (*S2OperatingPacketHandler::instance())
