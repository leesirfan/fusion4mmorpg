#include "preHeader.h"
#include "AccumulativeCharge.h"

AccumulativeCharge::AccumulativeCharge(const OperatingActivity* pProto)
: OperatingActivityBase(pProto, nSaveData)
{
}

AccumulativeCharge::~AccumulativeCharge()
{
}

GErrorCode AccumulativeCharge::OnPlayerGetReward(INetPacket& pck, const OperatingPlayerInfo& info, uint32 index)
{
	auto pActData = GetActivityData(info.playerId);
	if (pActData == NULL) {
		return ErrOperatingNotReachTarget;
	}

	if (index >= m_actvtParams.rewardDataList.size()) {
		return ErrOperatingInvalidReward;
	}

	auto& rewardData = m_actvtParams.rewardDataList[index];
	if (pActData->saveData[PayValue] < rewardData.gradVal) {
		return ErrOperatingNotReachTarget;
	}
	if (BIT_ISSET(pActData->saveData[DrawFlags], index)) {
		return ErrOperatingRewardGeted;
	}

	auto errCode = GetRewardItems(
		info, rewardData.rewardItems, false, 1, [=]() {
			BIT_CLR(pActData->saveData[DrawFlags], index);
			return true;
		});
	if (errCode != CommonSuccess) {
		return errCode;
	}

	BIT_SET(pActData->saveData[DrawFlags], index);
	SaveDataToDB(pActData->playerId);

	return CommonWaiting;
}

bool AccumulativeCharge::OnPlayerPay(const OperatingPlayerInfo& info, uint32 rmbVal, uint32 goldVal, uint32 payType)
{
	auto pActData = CreateAndGetActivityData(info.playerId);
	pActData->saveData[PayValue] += goldVal;
	SaveDataToDB(pActData->playerId);
	return true;
}

void AccumulativeCharge::OnDayEnd()
{
	std::vector<uint32> playerIds;
	for (auto& [playerId, pActData] : m_mapActData) {
		if (pActData->saveData[PayValue] == 0) { continue; }
		for (size_t i = 0, n = m_actvtParams.rewardDataList.size(); i < n; ++i) {
			auto& rewardData = m_actvtParams.rewardDataList[i];
			if (pActData->saveData[PayValue] >= rewardData.gradVal) {
				if (!BIT_ISSET(pActData->saveData[DrawFlags], (int)i)) {
					SendRewardMail(playerId, rewardData.rewardItems, 400200289, 400200290);
				}
			} else {
				break;
			}
		}
		ClearActivitySaveData(*pActData);
		playerIds.push_back(playerId);
	}
	SaveDataToDB(playerIds);
}

bool AccumulativeCharge::HasShortHot(const OperatingPlayerInfo& info) const
{
	auto pActData = GetActivityData(info.playerId);
	if (pActData == NULL || pActData->saveData[PayValue] == 0) {
		return false;
	}
	for (size_t i = 0, n = m_actvtParams.rewardDataList.size(); i < n; ++i) {
		auto& rewardData = m_actvtParams.rewardDataList[i];
		if (pActData->saveData[PayValue] >= rewardData.gradVal) {
			if (!BIT_ISSET(pActData->saveData[DrawFlags], (int)i)) {
				return true;
			}
		} else {
			break;
		}
	}
	return false;
}

void AccumulativeCharge::LoadActivityCares(std::bitset<(int)OperatingCareType::Count>& cares)
{
	cares.set((int)OperatingCareType::Pay);
}

void AccumulativeCharge::ReloadActivityParams()
{
	REINIT_OBJECT(&m_actvtParams);
	TextUnpacker unpacker(m_pProto->strActivityParams.c_str());
	do {
		OperatingAccumulativeCharge::RewardData rewardData;
		unpacker >> rewardData.gradVal;
		do {
			FItemInfo rewardItem;
			LoadConfigByFormat("uuu", rewardItem, unpacker);
			rewardData.rewardItems.push_back(rewardItem);
		} while (!unpacker.IsEmpty() && !unpacker.IsDelimiter('|'));
		m_actvtParams.rewardDataList.push_back(std::move(rewardData));
	} while (!unpacker.IsEmpty());
}

void AccumulativeCharge::BuildActivityParamsPacket(NetPacket& pck, const OperatingPlayerInfo& info)
{
	SaveToINetStream(m_actvtParams, pck);
}
