#pragma once

#include "Singleton.h"
#include "FrameWorker.h"
#include "Game/ServerService.h"
#include "OperatingActivityBase.h"

class OperatingActivityMgr : public FrameWorker, public AsyncTaskOwner,
	public WheelTimerOwner,
	public IServerService, public Singleton<OperatingActivityMgr>
{
public:
	THREAD_RUNTIME(OperatingActivityMgr)

	OperatingActivityMgr();
	virtual ~OperatingActivityMgr();

	virtual bool Initialize();
	virtual void Update(uint64 diffTime);
	virtual void OnTick();

	void ReloadData();
	void ReloadActivityUIType();

	void InitLastEventTime(time_t lastEventTime);

	void BuildActivityShortShotPacket(const OperatingPlayerInfo& info, uint64 actUniqueKey);
	void BuildActivityDetailPacket(const OperatingPlayerInfo& info, uint64 actUniqueKey);
	void BuildActivityListPacket(const OperatingPlayerInfo& info, int32 actUIType);
	void BuildTotalActivityTypePacket(uint32 playerId);

	void OnPlayerBuy(const OperatingPlayerInfo& info, uint64 actUniqueKey, uint32 index);
	void OnPlayerGetReward(const OperatingPlayerInfo& info, uint64 actUniqueKey, uint32 index);

	void OnPlayerLogin(const OperatingPlayerInfo& info);
	void OnPlayerPay(const OperatingPlayerInfo& info, uint32 rmbVal, uint32 goldVal, uint32 payType);
	void OnPlayerCost(const OperatingPlayerInfo& info, uint32 goldVal, uint32 costType);
	void OnPlayerGainScore(const OperatingPlayerInfo& info, uint32 scoreValue, uint32 scoreType);
	void OnPlayerPlayActvt(const OperatingPlayerInfo& info, OperatingPlayType playType, const uint32 params[]);
	void OnPlayerEventActvt(const OperatingPlayerInfo& info, OperatingEventType eventType, const uint32 params[]);

	static OperatingPlayerInfo UnpackPlayerInfo(INetStream& pck);

	WheelTimerMgr sWheelTimerMgr;

private:
	void CleanupSaveData();
	void UpdateAllActivityStatus();

	bool IsPreActivityFinished(const OperatingPlayerInfo& info, const OperatingActivityBase* pActInst) const;
	GErrorCode GetActivityStatus(const OperatingPlayerInfo& info, const OperatingActivityBase* pActInst) const;
	GErrorCode GetActivityShowStatus(const OperatingPlayerInfo& info, const OperatingActivityBase* pActInst) const;
	GErrorCode GetActivityListStatus(const OperatingPlayerInfo& info, const OperatingActivityBase* pActInst) const;

	void TryShowActivityShortHotStatus(const OperatingPlayerInfo& info, const OperatingActivityBase* pActInst) const;

	void RestartActivityTimer(bool isReset = false);
	time_t GetActivityNextEventTime() const;

	void BroadcastAllActivityCares() const;

	static void BroadcastActivityListDirty();

	static bool IsActivityExpired(const OperatingActivity& actProto);
	static bool IsActivityShowing(const OperatingActivity& actProto);
	static bool IsActivityPlaying(const OperatingActivity& actProto);

	virtual int HandleClientPacket(uint32 uid, INetPacket& pck);
	virtual int HandleServerPacket(INetPacket& pck);

	virtual WheelTimerMgr *GetWheelTimerMgr();

	std::unordered_map<uint64, OperatingActivityBase*> m_mapActInst;
	NetPacket m_actUITypePkt;
	time_t m_lastEventTime;
};

#define sOperatingActivityMgr (*OperatingActivityMgr::instance())
