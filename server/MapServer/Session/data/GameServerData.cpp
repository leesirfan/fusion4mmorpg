#include "preHeader.h"
#include "GameServerData.h"

GameServerData::GameServerData()
: m_playerLevelMax(0)
{
}

GameServerData::~GameServerData()
{
}

void GameServerData::SetPlayerLevelMax(uint32 playerLevelMax)
{
	m_playerLevelMax = playerLevelMax;
}

void GameServerData::SetOperatingCares(const std::string& operatingCares)
{
	m_operatingCares = std::bitset<(int)OperatingCareType::Count>(operatingCares);
}
