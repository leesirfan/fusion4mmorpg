#pragma once

#include "Singleton.h"
#include "MessageHandler.h"
#include "protocol/InternalProtocol.h"

class GateServerSession;

class GateServerSessionHandler :
	public MessageHandler<GateServerSessionHandler, GateServerSession, GateServer2MapServer::GATE2MAP_MESSAGE_COUNT>,
	public Singleton<GateServerSessionHandler>
{
public:
	GateServerSessionHandler();
	virtual ~GateServerSessionHandler();
private:
	int HandleRegister(GateServerSession *pSession, INetPacket &pck);
};

#define sGateServerSessionHandler (*GateServerSessionHandler::instance())
