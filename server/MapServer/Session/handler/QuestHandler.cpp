#include "preHeader.h"
#include "Session/PlayerPacketHandler.h"
#include "Map/MapInstance.h"
#include "Quest/QuestMgr.h"

int PlayerPacketHandler::HandleEnterQuestGuide(Player *pPlayer, INetPacket &pck)
{
	ObjGUID objGuid;
	uint32 questTypeID;
	pck >> objGuid >> questTypeID;
	uint32 questUniqueKey = 0;
	if (!pck.IsReadableEmpty()) {
		pck >> questUniqueKey;
	}
	auto pLObj = pPlayer->GetMapInstance()->GetLocatableObject(objGuid);
	auto errCode = sQuestMgr.HandleEnterQuestGuide(pPlayer, pLObj, questTypeID, questUniqueKey);
	if (errCode != CommonSuccess) {
		pPlayer->SendError(errCode);
	}
	return SessionHandleSuccess;
}

int PlayerPacketHandler::HandleQueryQuestGiver(Player *pPlayer, INetPacket &pck)
{
	ObjGUID objGuid;
	pck >> objGuid;
	auto pLObj = pPlayer->GetMapInstance()->GetLocatableObject(objGuid);
	sQuestMgr.SendObjCareNoWatchStatusQuests(pPlayer, pLObj);
	return SessionHandleSuccess;
}

int PlayerPacketHandler::HandleAcceptQuest(Player *pPlayer, INetPacket &pck)
{
	ObjGUID objGuid;
	uint32 questTypeID;
	pck >> objGuid >> questTypeID;
	auto pLObj = pPlayer->GetMapInstance()->GetLocatableObject(objGuid);
	auto pQuestProto = GetDBEntry<QuestPrototype>(questTypeID);
	auto errCode = sQuestMgr.HandleAcceptQuest(pPlayer, pLObj, pQuestProto);
	if (errCode != CommonSuccess) {
		pPlayer->SendError(errCode);
	}
	return SessionHandleSuccess;
}

int PlayerPacketHandler::HandleCancelQuest(Player *pPlayer, INetPacket &pck)
{
	ObjGUID objGuid;
	uint32 questUniqueKey;
	pck >> objGuid >> questUniqueKey;
	auto pLObj = pPlayer->GetMapInstance()->GetLocatableObject(objGuid);
	auto errCode = sQuestMgr.HandleCancelQuest(pPlayer, pLObj, questUniqueKey);
	if (errCode != CommonSuccess) {
		pPlayer->SendError(errCode);
	}
	return SessionHandleSuccess;
}

int PlayerPacketHandler::HandleSubmitQuest(Player *pPlayer, INetPacket &pck)
{
	ObjGUID objGuid;
	uint32 questUniqueKey;
	pck >> objGuid >> questUniqueKey;
	auto pLObj = pPlayer->GetMapInstance()->GetLocatableObject(objGuid);
	auto errCode = sQuestMgr.HandleSubmitQuest(pPlayer, pLObj, questUniqueKey);
	if (errCode != CommonSuccess) {
		pPlayer->SendError(errCode);
	}
	return SessionHandleSuccess;
}

int PlayerPacketHandler::HandleQueryQuestNavigation(Player *pPlayer, INetPacket &pck)
{
	uint32 questUniqueKey;
	int8 navIdx;
	pck >> questUniqueKey >> navIdx;
	auto pQuestLog = pPlayer->GetQuestStorage()->GetQuestLogByKey(questUniqueKey);
	if (pQuestLog != NULL) {
		pQuestLog->QueryQuestNavData(navIdx);
	}
	return SessionHandleSuccess;
}
