#include "preHeader.h"
#include "Session/PlayerPacketHandler.h"
#include "Map/MapInstance.h"

int PlayerPacketHandler::HandleSpellCast(Player *pPlayer, INetPacket &pck)
{
	uint32 spellID;
	uint64 spellInstGuid;
	ObjGUID targetGuid;
	vector3f tgtEffPos, selfPos, selfDir;
	pck >> spellID >> spellInstGuid >> targetGuid >> tgtEffPos;
	pck >> selfPos >> selfDir;

	pPlayer->SetPosition(selfPos);
	pPlayer->SetDirection(selfDir);

	auto pTarget = pPlayer->GetMapInstance()->GetLocatableObject(targetGuid);
	auto errCode = pPlayer->CastSpell2Target(pTarget, spellID, spellInstGuid, tgtEffPos);
	NetPacket resp(SMSG_SPELL_CAST_RESP);
	resp << spellInstGuid << errCode;
	pPlayer->SendPacket(resp);

	return SessionHandleSuccess;
}

int PlayerPacketHandler::HandleSpellInterrupt(Player *pPlayer, INetPacket &pck)
{
	uint64 spellInstGuid;
	pck >> spellInstGuid;
	pPlayer->InterruptSpellByGuid(spellInstGuid, false);
	NetPacket resp(SMSG_SPELL_INTERRUPT_RESP);
	resp << spellInstGuid;
	pPlayer->SendPacket(resp);
	return SessionHandleSuccess;
}
