#pragma once

#include "IMapHook.h"

class GuildLeagueHook : public IMapHook
{
public:
	GuildLeagueHook(MapInstance* pMapInstance);
	virtual ~GuildLeagueHook();

	virtual void InitLuaEnv();

	virtual void InitArgs(const std::string& args);

	virtual vector3f1f GetEntryPoint(
		uint32 gsIdx, const CharTeleportInfo& tpInfo) const;

private:
	ArenaTeamSide GetTeamSide(uint32 guildId) const;
	int64 TryGetWinGuildId(uint32 loseTeamSide) const;
	int64 TryGetWinGuildId4Opening() const;
	uint32 JudgeWinGuildId() const;

	vector3f1f m_entryPoints[teamCount];
	uint32 m_guildIds[teamCount];
};
