#pragma once

#include "DetourNavMeshQuery.h"

#define NAV_PATH_MAX_POLYS (1024)

class Scene
{
public:
	Scene();
	~Scene();

	bool Init(const std::string& sceneDir);

	int FindStraightPath(const vector3f& startPos, const vector3f& endPos,
		vector3f straightPath[], int maxStraightPathSize, int excludeFlags = 0);

	int Raycast(const vector3f& startPos, const vector3f& endPos,
		vector3f& hitPos, int excludeFlags = 0);

	const aabb2f& GetSceneBound() const { return m_sceneBound; }

private:
	bool LoadNavMesh(const std::string& sceneDir);
	bool LoadSceneBound(const std::string& sceneDir);

	dtNavMeshQuery* GetNavMeshQuery();
	void PutNavMeshQuery(dtNavMeshQuery* pNavMeshQuery);

	static std::string GetSceneBoundFile(const std::string& sceneDir);
	static std::string GetNavMeshFile(const std::string& sceneDir);

	aabb2f m_sceneBound;

	dtNavMesh* m_navMesh;
	ThreadSafePool<dtNavMeshQuery, 128> m_navMeshQueryPool;

	static const float m_polyPickExtents[2][3];
};
