#pragma once

class Player;

class VipStorage
{
public:
	VipStorage(Player* pOwner);
	~VipStorage();

	void OnPay(uint32 payId, time_t payTime);

	GErrorCode GainExp(uint64 value,
		CHEQUE_FLOW_TYPE flowType, params<uint32> flowParams);

	void BuildCreatePacketForPlayer(INetPacket& pck, Player* pPlayer);

	std::string Save() const;
	void Load(const std::string& data);

	uint32 GetLevel() const { return m_level; }

private:
	void RefreshLevel();

	Player* const m_pOwner;

	uint32 m_level;
	uint64 m_exp;
};
