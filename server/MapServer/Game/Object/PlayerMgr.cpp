#include "preHeader.h"
#include "PlayerMgr.h"

PlayerMgr::PlayerMgr()
{
}

PlayerMgr::~PlayerMgr()
{
}

void PlayerMgr::AddPlayer(ObjGUID guid, InstGUID instGuid)
{
	std::lock_guard<std::shared_mutex> lock(m_playerSharedMutex);
	m_playerInstances[guid] = instGuid;
}

void PlayerMgr::RemovePlayer(ObjGUID guid)
{
	std::lock_guard<std::shared_mutex> lock(m_playerSharedMutex);
	m_playerInstances.erase(guid);
}

InstGUID PlayerMgr::GetPlayerInstance(ObjGUID guid) const
{
	std::shared_lock<std::shared_mutex> lock(m_playerSharedMutex);
	auto itr = m_playerInstances.find(guid);
	return itr != m_playerInstances.end() ? itr->second : InstGUID_NULL;
}
