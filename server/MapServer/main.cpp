#include "preHeader.h"
#include "MapServerMaster.h"

int main(int argc, char *argv[])
{
	MapServerMaster::newInstance();
	sMapServerMaster.InitSingleton();

	if (sMapServerMaster.Initialize(argc, argv) == 0) {
		sMapServerMaster.Run(argc, argv);
	}

	sMapServerMaster.FinishSingleton();
	MapServerMaster::deleteInstance();

	printf("Map Server shutdown gracefully!\n");

	return 0;
}
