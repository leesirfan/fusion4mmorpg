#pragma once

#include "ServerService.h"

class MapServerService : public IServerService
{
public:
	MapServerService();
	virtual ~MapServerService();

protected:
	virtual void OnRecvPacket(INetPacket* pck);

	virtual int HandlePacket(INetPacket& pck);

private:
	void TransInstancePacket(INetPacket* pck) const;
	void TransPlayerPacket(INetPacket* pck) const;
	void TransClientPacket(INetPacket* pck) const;
	void TransGameServerPacket(INetPacket* pck) const;
};
