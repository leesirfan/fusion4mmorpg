-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- 主机： db
-- 生成日期： 2020-07-08 12:03:20
-- 服务器版本： 8.0.20
-- PHP 版本： 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `mmorpg_global`
--

-- --------------------------------------------------------

--
-- 表的结构 `t_accounts`
--

CREATE TABLE `t_accounts` (
  `Id` int UNSIGNED NOT NULL,
  `username` char(32) NOT NULL,
  `password` char(32) NOT NULL,
  `createTime` bigint NOT NULL,
  `banExpireTime` bigint NOT NULL,
  `lastLoginGsId` int UNSIGNED NOT NULL,
  `lastLoginTime` bigint NOT NULL,
  `allowCheat` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- 表的结构 `t_account_characters`
--

CREATE TABLE `t_account_characters` (
  `accountId` int UNSIGNED NOT NULL,
  `serverId` int UNSIGNED NOT NULL,
  `characterId` int UNSIGNED NOT NULL,
  `characterName` varchar(256) NOT NULL,
  `characterLevel` int UNSIGNED NOT NULL,
  `createTime` bigint UNSIGNED NOT NULL,
  `deleteTime` bigint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- 表的结构 `t_game_servers`
--

CREATE TABLE `t_game_servers` (
  `Id` int UNSIGNED NOT NULL,
  `externalIP` char(46) NOT NULL,
  `externalPort` smallint UNSIGNED NOT NULL,
  `internalIP` char(46) NOT NULL,
  `internalName` char(32) NOT NULL,
  `logicId` int UNSIGNED NOT NULL,
  `logicName` char(32) NOT NULL,
  `logicOpenStatus` tinyint UNSIGNED NOT NULL,
  `logicSpecialFlags` int UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- 转存表中的数据 `t_game_servers`
--

INSERT INTO `t_game_servers` (`Id`, `externalIP`, `externalPort`, `internalIP`, `internalName`, `logicId`, `logicName`, `logicOpenStatus`, `logicSpecialFlags`) VALUES
(1, '127.0.0.1', 9999, '127.0.0.1', 'main', 0, '', 0, 0);

-- --------------------------------------------------------

--
-- 表的结构 `t_guilds`
--

CREATE TABLE `t_guilds` (
  `Id` int UNSIGNED NOT NULL,
  `guildName` varchar(256) NOT NULL,
  `createGsId` int UNSIGNED NOT NULL,
  `createPlayer` int UNSIGNED NOT NULL,
  `createTime` bigint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- 转储表的索引
--

--
-- 表的索引 `t_accounts`
--
ALTER TABLE `t_accounts`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- 表的索引 `t_account_characters`
--
ALTER TABLE `t_account_characters`
  ADD PRIMARY KEY (`characterId`),
  ADD UNIQUE KEY `characterName` (`characterName`),
  ADD KEY `accountId` (`accountId`,`serverId`);

--
-- 表的索引 `t_game_servers`
--
ALTER TABLE `t_game_servers`
  ADD PRIMARY KEY (`Id`);

--
-- 表的索引 `t_guilds`
--
ALTER TABLE `t_guilds`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `guildName` (`guildName`) USING BTREE;

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `t_accounts`
--
ALTER TABLE `t_accounts`
  MODIFY `Id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1000;

--
-- 使用表AUTO_INCREMENT `t_account_characters`
--
ALTER TABLE `t_account_characters`
  MODIFY `characterId` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- 使用表AUTO_INCREMENT `t_game_servers`
--
ALTER TABLE `t_game_servers`
  MODIFY `Id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `t_guilds`
--
ALTER TABLE `t_guilds`
  MODIFY `Id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
