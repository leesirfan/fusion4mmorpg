# -*- coding: utf-8 -*-

import os
import xml.etree.cElementTree as ET

ns = {'xmlns': 'http://schemas.microsoft.com/developer/msbuild/2003'}

def load(file):
    return ET.parse(file)

def dump(tree, libName, outPath, fixPath, CXXIncludes):
    sources = []
    root = tree.getroot()
    for node in root.iterfind('xmlns:ItemGroup/xmlns:ClCompile', ns):
        sources.append(node.attrib['Include'])
    os.makedirs(os.path.join(outPath,libName), exist_ok=True)
    with open(os.path.join(outPath,libName,'CMakeLists.txt'),'w') as fp:
        fp.write('include_directories(%s %s\n' %
            ('${3RD_INCLUDE_DIRS}', '${Boost_INCLUDE_DIRS}'))
        for include in CXXIncludes:
            fp.write('\t%s\n' % include)
        fp.write(')\n\n')
        fp.write('add_library(%s STATIC\n' % libName)
        for source in sources:
            fp.write('\t%s\n' % os.path.abspath(
                os.path.join(fixPath, source.replace('\\', '/'))))
        fp.write(')')
