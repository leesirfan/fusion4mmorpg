# -*- coding: utf-8 -*-

import os
from vcxproj2CMake import *

CXXIncludes = []

FusionIncludes = [
    'fusion/external',
    'fusion/feature',
    'fusion/game',
    'fusion/src',
    'fusion/thirdparty',
]

GameDataIncludes = [
    'GameData',
    'GameData/thirdparty',
    'GameData/thirdparty/Detour/Include',
]

GameBaseIncludes = [
    'GameBase',
]

DBPServerIncludes = [
    'DBPServer',
]

GateServerIncludes = [
    'GateServer',
]

GameServerIncludes = [
    'GameServer',
]

MapServerIncludes = [
    'MapServer',
    'MapServer/Game',
]

SocialServerIncludes = [
    'SocialServer',
]

def FixProjectInclues():
    fixPath = '../../'
    for i in range(len(FusionIncludes)):
        FusionIncludes[i] = os.path.abspath(os.path.join(fixPath, FusionIncludes[i]))
    for i in range(len(GameDataIncludes)):
        GameDataIncludes[i] = os.path.abspath(os.path.join(fixPath, GameDataIncludes[i]))
    for i in range(len(GameBaseIncludes)):
        GameBaseIncludes[i] = os.path.abspath(os.path.join(fixPath, GameBaseIncludes[i]))
    for i in range(len(DBPServerIncludes)):
        DBPServerIncludes[i] = os.path.abspath(os.path.join(fixPath, DBPServerIncludes[i]))
    for i in range(len(GateServerIncludes)):
        GateServerIncludes[i] = os.path.abspath(os.path.join(fixPath, GateServerIncludes[i]))
    for i in range(len(GameServerIncludes)):
        GameServerIncludes[i] = os.path.abspath(os.path.join(fixPath, GameServerIncludes[i]))
    for i in range(len(MapServerIncludes)):
        MapServerIncludes[i] = os.path.abspath(os.path.join(fixPath, MapServerIncludes[i]))
    for i in range(len(SocialServerIncludes)):
       SocialServerIncludes[i] = os.path.abspath(os.path.join(fixPath, SocialServerIncludes[i]))
    FusionIncludes.extend(CXXIncludes)
    GameDataIncludes.extend(FusionIncludes)
    GameBaseIncludes.extend(GameDataIncludes)
    DBPServerIncludes.extend(GameBaseIncludes)
    GateServerIncludes.extend(GameBaseIncludes)
    GameServerIncludes.extend(GameBaseIncludes)
    MapServerIncludes.extend(GameBaseIncludes)
    SocialServerIncludes.extend(GameBaseIncludes)

def DumpProject():
    outPath, fixPath = 'build', '.'
    dump(load('../win/fusion.vcxproj'), 'fusion', outPath, fixPath, FusionIncludes)
    dump(load('../win/GameBase.vcxproj'), 'GameBase', outPath, fixPath, GameBaseIncludes)
    dump(load('../win/GameData.vcxproj'), 'GameData', outPath, fixPath, GameDataIncludes)
    dump(load('../win/DBPServer.vcxproj'), 'DBPServer', outPath, fixPath, DBPServerIncludes)
    dump(load('../win/GateServer.vcxproj'), 'GateServer', outPath, fixPath, GateServerIncludes)
    dump(load('../win/GameServer.vcxproj'), 'GameServer', outPath, fixPath, GameServerIncludes)
    dump(load('../win/MapServer.vcxproj'), 'MapServer', outPath, fixPath, MapServerIncludes)
    dump(load('../win/SocialServer.vcxproj'), 'SocialServer', outPath, fixPath, SocialServerIncludes)
