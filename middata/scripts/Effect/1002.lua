local t, t1, arg = {}, Class(), nil

function main(pSpell, i, key, effArgs, extArgs)
	arg = ParseSpellEffArgs(key, effArgs, t.ParseEffArgs)
	pSpell:SaveEffectBuffInfos(i, t1)
	pSpell:SaveEffectTable(i, t, t1)
end

function t.ParseEffArgs(arg, effArgs)
	arg.duration, arg.interval, arg.attackRate, arg.attackValue =
		table.unpack(effArgs:splitnumber(','))
end

function t.ApplyEffect(pTarget)
	local tx = t1:new({obj=pTarget, duration=t1.duration or arg.duration})
	pTarget:AttachSpellBuffInfo(tx)
end

function t1.OnAttach(tx, key)
	local restTimes = CalcSpellBuffTriggerTimes(tx.duration, arg.interval)
	tx.handler = CreateHandlerTimerWithDtor(tx.obj, function()
		if CheckObjectAlive(t1.caster) then
			tx:ApplyEffect()
		else
			RemoveSpellBuffTimer(tx)
		end
	end, function()
		TryDetachSpellBuffAsTimerDtor(tx, key)
	end, arg.interval, math.max(restTimes, 0))
end

function t1.OnDetach(tx)
	TryRemoveTimerWhenSpellBuffDetached(tx)
end

function t1.ApplyEffect(tx)
	t1.caster:Strike(tx.obj, tx)
end

function t1.CalcDamage(tx, pVictim)
	return tx.obj:GetAttribute():CalcAttackDamage(pVictim, arg.attackRate, arg.attackValue)
end